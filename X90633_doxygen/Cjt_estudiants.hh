/** @file Cjt_estudiants.hh
    @brief Especificació de la clase Conjunt d'estudiants
*/
#ifndef CONJ_EST_HH
#define CONJ_EST_HH

#include "Estudiant.hh"
#include <vector>

/** @class Cjt_estudiants
    @brief Representa un conjunt d'estudiants ordenat per DNI.

    Cada estudiant té un DNI únic, i pot tenir o no tenir nota. El conjunt està ordenat creixentment pel DNI dels estudiants
*/
class Cjt_estudiants {

private:

  vector<Estudiant> vest;
  int nest;
  static const int MAX_NEST = 20; // nombre maxim d'elements del conjunt
  int nest_amb_nota;
  double suma_notes;


  void ordenar_cjt_estudiants();
  /* Pre: cert */
  /* Post: el elements del paràmetre implícit passen a estar ordenats
     creixentment per DNI. */

  static int cerca_dicot(const vector<Estudiant> &vest, int left, int right, int x);
  /* Pre: els elements de vest estan ordenats creixentment per DNI,
     0<=left, right<vest.size(), right>=left-1 */
  /* Post: si x hi és vest[left..right], el resultat és la posició que
     hi ocupa, si no, és la posició que hauria d'ocupar */


public:

  //Constructores

  /** @brief Creadora per defecte
    \pre <em>cert</em>
    \post crea un conjunt d'estudiants buit
  */
  Cjt_estudiants();


  //Modificadores

  /** @brief Afegeix un estudiant al conjunt
      \pre el paràmetre implícit no està ple
      \post b = indica si el p.i. original conté un estudiant amb el dni d'est; si b = fals, s'ha afegit l'estudiant est al paràmetre implícit
  */
  void afegir_estudiant(const Estudiant &est, bool& b);


  /** @brief Esborra un estudiant del conjunt
      \pre <em>cert</em>
      \post b indica si el paràmetre implícit original tenia un estudiant amb el dni dni; si b, aquest estudiant ha quedat eliminat del paràmetre implícit
  */
  void esborrar_estudiant(int dni, bool& b);


  //Consultores

  /** @brief Consultora de la mida actual del conjunt
      \pre <em>cert</em>
      \post El resultat és el nombre d'estudiants del paràmetre implícit
  */
  int mida() const;

  /** @brief Consultora de la mida màxima del conjunt
      \pre <em>cert</em>
      \post el resultat es el nombre maxim d'estudiants que pot arribar a tenir el parametre implicit
  */
  static int mida_maxima();

  /** @brief Consultora de la mitjana dels estudiants amb nota
      \pre <em>cert</em>
      \post el resultat és la mitjana de les notes dels estudiants amb nota del paràmetre implícit; si no n'hi ha cap, el resultat és -1
  */
  double mitjana_estudiants_amb_nota() const;


  // Lectura i escriptura

  /** @brief Operació de lectura del conjunt d'estudiants
      \pre <em>cert</em>
      \post el paràmetre implícit conté el conjunt d'estudiants llegits del canal estàndar d'entrada
  */
  void llegir();

  /** @brief Operació d'escriptura del conjunt d'estudiants
      \pre <em>cert</em>
      \post s'han escrit pel canal estàndar de sortida els estudiants del conjunt que conté el paràmetre implícit en ordre ascendent per DNI
  */
  void escriure() const;

};

#endif
