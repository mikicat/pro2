/** @file Estudiant.hh
    @brief Especificació de la clase Estudiant
*/
#ifndef ESTUDIANT_HPP
#define ESTUDIANT_HPP

#include "utils.PRO2"

/** @class Estudiant
    @brief Representa un Estudiant

    L'Estudiant consta d'un DNI i pot tenir nota, que seria un double.
    Les notes vàlides són de l'interval 0..nota_maxima()
*/
class Estudiant {

private:
  int dni;
  double nota;
  bool amb_nota;
  static const int MAX_NOTA = 10;
  /*
     Invariant de la representació:
     - 0 <= dni
     - si amb_nota, llavors 0 <= nota <= MAX_NOTA
  */

public:
  //Constructores

  /** @brief Creadora per defecte
      \pre <em>cert</em>
      \post el resultat és un estudiant amb DNI = 0 i sense nota
  */
  Estudiant();

  /** @brief Creadora amb valors concrets de DNI
      \pre dni >= 0
      \post el resultat és un estudiant amb DNI=dni i sense nota
  */
  Estudiant(int dni);


  // Destructora:

  /** @brief Destructora per defecte

      esborra automàticament els objectes locals en sortir
      d'un àmbit de visibilitat
  */
  ~Estudiant();

  //Modificadores

  /** @brief Afegeix una nota a l'estudiant
      \pre el paràmetre implícit no té nota, 0 <= "nota" <= nota_maxima()
      \post la nota del paràmetre implícit passa a ser "nota"
  */
  void afegir_nota(double nota);

  /** @brief Modifica la nota de l'estudiant
      |pre el paràmetre implícit té nota, 0 <= "nota" <= nota_maxima()
      \post la nota del paràmetre implícit passa a ser "nota"
  */
  void modificar_nota(double nota);


  //Consultores

  /** @brief Consultora del DNI de l'estudiant
      \pre <em>cert</em>
      \post el resultat és el DNI del paràmetre implícit
  */
  int consultar_DNI() const;

  /** @brief Consultora de la nota de l'estudiant
      \pre el paràmetre implícit té nota
      \post el resultat és la nota del paràmetre implícit
  */
  double consultar_nota() const;

  /** @brief Consultora de la nota màxima que pot tenir l'estudiant
      \pre <em>cert</em>
      \post el resultat és la nota màxima dels elements de la classe
  */
  static double nota_maxima();

  /** @brief Consultora de si l'estudiant té nota
      \pre <em>cert</em>
      \post el resultat indica si el paràmetre implícit té nota o no
  */
  bool te_nota()  const;

  /** @brief Operació de comparació entre dos estudiants
      \pre <em>cert</em>
      \post el resultat indica si el DNI d'e1 es més petit que el d'e2
  */
  static bool comp(const Estudiant& e1, const Estudiant& e2);


  // Lectura i escriptura

  /** @brief Operació de lectura del conjunt d'estudiants
      \pre  hi ha preparats al canal estandar d'entrada un enter no negatiu i un double
      \post el paràmetre implícit passa a tenir els atributs llegits
     del canal estàndard d'entrada; si el double no pertany a l'interval
     [0..nota_maxima()], el p.i. es queda sense nota
  */
  void llegir();

  /** @brief Operació d'escriptura del conjunt d'estudiants
  /* Pre: cert */
  /* Post: s'han escrit els atributs del paràmetre implícit
     al canal estàndard de sortida; si no té nota escriu "NP" */
  void escriure() const;

};
#endif
