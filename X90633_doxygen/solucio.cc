#include "Cjt_estudiants.hh"

void Cjt_estudiants::afegir_estudiant(const Estudiant &est, bool& b)
{
  int dni = est.consultar_DNI();
  b = false;
  int i = 0;
  while (i < nest and not b) {
    int dni2 = vest[i].consultar_DNI();
    if (dni == dni2) b = true;
    ++i;
  }

  if (not b) {
    i = nest-1;
    bool b2 = false;

    while (i >= 0 and not b2) {
      if (dni > vest[i].consultar_DNI()) b2 = true;
      else {
        vest[i+1] = vest[i];
        --i;
      }
    }
    vest[i+1] = est;
    if (est.te_nota()) ++nest_amb_nota, suma_notes += est.consultar_nota();
    ++nest;
  }
}

void Cjt_estudiants::esborrar_estudiant(int dni, bool& b)
{
  int x = 0;
  b = false;
  while (x < nest and not b) {
    if (vest[x].consultar_DNI() == dni) b = true;
    else ++x;
  }
  if (b) {
    if (vest[x].te_nota()) --nest_amb_nota, suma_notes -= vest[x].consultar_nota();

    for (int j = x; j < nest - 1; ++j) {
      vest[j] = vest[j + 1];
    }
    --nest;
  }
}
