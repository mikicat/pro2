#include <iostream>

/* APUNTS 18/2/20

Tipus operacions: [Diapo 32]
Creadores -> creen objectes nous
  - Tenen mateix nom q la classe
  - No paràmetre implícit
Constructora per defecte -> sense paràmetres

Destructora:
  ~nom_classe() { ... }

Modificadores:
  - Tipus void
  - Transformen el paràmetre implícit (obj)

Consultores:
  - Proporcionen info sobre obj
  - De vegades poden retornar resultat mitjançant paràmentres de sortida (&)

Mètodes de classe:
  - Propis de la classe
  - NO paràmetre implícit
  - Poden accedir a tots els objectes

25/2
Atributs -> sempre privats
  - const -> no modificable
  - static -> atribut de classe

Operacions privades:
  - declarades dins del 'private' al Header
  - Mirar invariants amb representació [Diapo 74]
  - Exemple d'operació privada: [Diapo 78]
      static int cerca_dicot(const vector<Estudiant>& vest,
                              int left, int right, int x);
      -> Quan es construeix la classe al fitxer .cc no es posa 'static'
      simplement, Classe::funció (){}
  - Cal posar els pre/post
  - 


*/

class Estudiant {
public:
  Estudiant(); // Constructora per defecte
  Estudiant(int DNI); // Constructora amb paràmetre
  ~Estudiant(); // Destructora --> Gairebé no es fa servir explícitament
}
