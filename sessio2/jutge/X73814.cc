#include <iostream>
#include <vector>
#include "Estudiant.hh"

/* Leer vector */
void leer_vector(vector<Estudiant>& v) {
  int numEst;
  cin >> numEst;
  v = vector<Estudiant> (numEst);
  
  for (int i=0; i<numEst;++i)
    v[i].llegir();
}

/* Escribir vector */
void escribir_vector(const vector<Estudiant>& v) {
  for (int i=0; i<v.size();++i)
   v[i].escriure();
}

void simplificar(vector<Estudiant>& v) {
    vector<Estudiant> res;
    res.push_back(v[0]);
    
    for (int i = 1; i < v.size(); ++i) {
        bool same = false;
        int j = 0;
        while(j < res.size() and not same) {
            if (res[j].consultar_DNI() == v[i].consultar_DNI()) {
                same = true;
                if (v[i].te_nota() and res[j].te_nota()) {
                    if (v[i].consultar_nota() > res[j].consultar_nota()) {
                        res[j].modificar_nota(v[i].consultar_nota());
                    }
                }
                else if (v[i].te_nota()) res[j].afegir_nota(v[i].consultar_nota());
            }
            ++j;
        }
        if (j == res.size() and not same) {
            res.push_back(v[i]);
        }
    }
    v = res;
}


int main() {
    vector<Estudiant> v;
    leer_vector(v);
    simplificar(v);
    escribir_vector(v);
}
