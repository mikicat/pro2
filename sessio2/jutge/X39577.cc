#include <iostream>
#include <vector>
#include "Estudiant.hh"

Estudiant process_student(const int n, const vector<int>& b) {
  int dni;
  cin >> dni;
  Estudiant est(dni);
  vector<double> notes(n);
  for (int i = 0; i < n; ++i) cin >> notes[i];

  double av = 0;
  for (int i = 0; i < b.size(); ++i) {
    av += notes[b[i]];
  }
  av /= b.size();
  est.afegir_nota(av);

  return est;
}

int main() {
  int m, n, s;
  cin >> m >> n >> s;
  vector<int> b(s);
  for (int i = 0; i < s; ++i) {
    cin >> b[i];
    --b[i];
  }

  for (int i = 0; i < m; ++i) {
    Estudiant est = process_student(n, b);
    cout << est.consultar_DNI() << ' ' << est.consultar_nota() << endl;
  }
}
