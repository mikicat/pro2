#include "Cjt_estudiants.hh"

void actualitzar(Cjt_estudiants& cjt, const Cjt_estudiants& nou) {
  for (int i = 1; i <= nou.mida(); ++i) {
    int dni = nou.consultar_iessim(i).consultar_DNI();
    if (cjt.existeix_estudiant(dni)) {
      if (cjt.consultar_estudiant(dni).te_nota() and nou.consultar_iessim(i).te_nota()) {
        if (cjt.consultar_estudiant(dni).consultar_nota() < nou.consultar_iessim(i).consultar_nota()) {
          cjt.modificar_estudiant(nou.consultar_iessim(i));
        }
      }
      else if (nou.consultar_iessim(i).te_nota()) {
        cjt.modificar_estudiant(nou.consultar_iessim(i));
      }
    }
    else {
      cjt.afegir_estudiant(nou.consultar_iessim(i));
    }
  }
}

int main() {
  Cjt_estudiants cjt, nou;
  cjt.llegir();
  nou.llegir();
  actualitzar(cjt, nou);
  cjt.escriure();
}
