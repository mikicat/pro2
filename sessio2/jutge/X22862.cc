#include <iostream>
#include "Cjt_estudiants.hh"

double redondear(double r)
{
  return int(10.*(r + 0.05)) / 10.0;
}

void afegir_estudiant(Cjt_estudiants& cjt) {
  Estudiant est;
  est.llegir();
  if (cjt.existeix_estudiant(est.consultar_DNI())) {
    cout << "el estudiante " << est.consultar_DNI() << " ya estaba" << endl << endl;
  }
  else if (cjt.mida() == cjt.mida_maxima()) cout << "el conjunto esta lleno" << endl << endl;
  else cjt.afegir_estudiant(est);
}

void consultar(const Cjt_estudiants& cjt) {
  int dni;
  cin >> dni;
  if (cjt.existeix_estudiant(dni)) {
    if (cjt.consultar_estudiant(dni).te_nota()) cout << "el estudiante " << dni << " tiene nota " << cjt.consultar_estudiant(dni).consultar_nota() << endl;
    else cout << "el estudiante " << dni << " no tiene nota" << endl;
  }
  else cout << "el estudiante " << dni << " no esta" << endl;

  cout << endl;
}

void modificar(Cjt_estudiants& cjt) {
  Estudiant est;
  est.llegir();
  int dni = est.consultar_DNI();

  if (cjt.existeix_estudiant(dni)) cjt.modificar_estudiant(est);
  else cout << "el estudiante " << dni << " no esta" << endl << endl;
}

void arrodonir(Cjt_estudiants& cjt) {
  for (int i = 1; i <= cjt.mida(); ++i) {
    if (cjt.consultar_iessim(i).te_nota()) {
      Estudiant tmp = cjt.consultar_iessim(i);
      tmp.modificar_nota(redondear(tmp.consultar_nota()));

      cjt.modificar_iessim(i, tmp);
    }
  }
}

int main() {
  Cjt_estudiants cjt;
  cjt.llegir();

  int n;
  cin >> n;
  while (n != -6) {
    if (n == -1) afegir_estudiant(cjt);
    else if (n == -2) consultar(cjt);
    else if (n == -3) modificar(cjt);
    else if (n == -4) arrodonir(cjt);
    else if (n == -5) {cjt.escriure(); cout << endl;}

    cin >> n;
  }
}

/*
-1: afegir estudiant
-2: consultar nota a partir de dni
-3: modificar nota estudiant
-4: arrodonir nota a tots els estudiants
-5: escriure el conjunt
-6: fi
*/
