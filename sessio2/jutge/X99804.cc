#include <iostream>
#include <vector>
#include <algorithm>
#include "Estudiant.hh"

bool order(const Estudiant& a, const Estudiant& b) {
  if (a.consultar_nota() == b.consultar_nota()) return a.consultar_DNI() < b.consultar_DNI();
  return a.consultar_nota() > b.consultar_nota();
}

Estudiant process_student(const int n, const vector<int>& b) {
  int dni;
  cin >> dni;
  Estudiant est(dni);
  vector<double> notes(n);
  for (int i = 0; i < n; ++i) cin >> notes[i];

  double av = 0;
  for (int i = 0; i < b.size(); ++i) {
    av += notes[b[i]];
  }
  av /= b.size();
  est.afegir_nota(av);

  return est;
}

void print(vector<Estudiant>& vec) {
  for (int i = 0; i < vec.size(); ++i) {
    cout << vec[i].consultar_DNI() << ' ' << vec[i].consultar_nota() << endl;
  }
}

int main() {
  int m, n, s;
  cin >> m >> n >> s;
  vector<int> b(s);
  for (int i = 0; i < s; ++i) {
    cin >> b[i];
    --b[i];
  }

  vector<Estudiant> vec(m);
  for (int i = 0; i < m; ++i) {
    Estudiant est = process_student(n, b);
    vec[i] = est;
  }
  sort(vec.begin(), vec.end(), order);
  print(vec);
}
