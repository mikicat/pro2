#include "vectorIOEstudiant.hh"
/* Leer vector */
void leer_vector(vector<Estudiant>& v) {// version accion
  cout<<"Escribe el tamaño del vector:"<<endl;
  int numEst;
  cin >> numEst;
  v = vector<Estudiant> (numEst);
  
  cout<<"Escribe los elementos del vector:"<<endl;  
  for (int i=0; i<numEst;++i)
    v[i].llegir();
}

/* Escribir vector */
void escribir_vector(const vector<Estudiant>& v) {
  for (int i=0; i<v.size();++i)
   v[i].escriure();
  cout << endl;
}

// Redondear, version accion

void redondear_vector(vector<Estudiant>& v)
/* Pre: est tiene nota */
/* Post: est pasa a tener su nota original redondeada */
{
    for (int i=0; i<v.size();++i) {
        if (v[i].te_nota()) {
            v[i].modificar_nota(((int) (10. * (v[i].consultar_nota() + 0.05))) / 10.0);
        }
    }
}
