#include "BinTreeIOParInt.hh"
using namespace std;


int cerca (const BinTree<ParInt>& a, int x, int& c) {
  if (not a.empty()) {
    if (a.value().primer() == x) {
      c = a.value().segon();
      return 0;
    }
    else {
      int left = cerca(a.left(), x, c);
      if (left != -1) return 1 + left;
      int right = cerca(a.right(), x, c);
      if (right != -1) return 1 + right;
    }
  }
  return -1;
}

int main() {
  BinTree<ParInt> a;
  read_bintree_parint(a);
  int x, c;
  while (cin >> x) {
    int p = cerca(a, x, c);
    if (p != -1) cout << x << ' ' << c << ' ' << p << endl;
    else cout << -1 << endl;
  }
}
