#include "BinTreeIOParInt.hh"
using namespace std;

void read_bintree_parint(BinTree<ParInt>& a) {
  ParInt par;
  if (par.llegir()) {
    BinTree<ParInt> left;
    read_bintree_parint(left);
    BinTree<ParInt> right;
    read_bintree_parint(right);
    BinTree<ParInt> aux(par, left, right);

    a = aux;
  }
}
