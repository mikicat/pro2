#include "LlistaIOParInt.hh"

using namespace std;

int main() {
  list<ParInt> l;
  LlegirLlistaParInt(l);
  int n;
  cin >> n;

  int sum = 0;
  int cases = 0;

  for (list<ParInt>::const_iterator it = l.begin(); it != l.end(); ++it) {
    if ((*it).primer() == n) {
      ++cases;
      sum += (*it).segon();
    }
  }

  cout << n << ' ' << cases << ' ' << sum << endl;
}
