#include "BinTreeIOEst.hh"
using namespace std;

int cerca(const BinTree<Estudiant>& a, const int& dni, double& nota) {
  if (not a.empty()) {
    if (a.value().consultar_DNI() == dni) {
      if (a.value().te_nota()) nota = a.value().consultar_nota();
      else nota = -1;
      return 0;
    }

    int left = cerca(a.left(), dni, nota);
    double nota_dreta;
    int right = cerca(a.right(), dni, nota_dreta);
    if (left != -1 or right != -1) {
      if (left != -1 and right != -1) {
        if (right < left) {
          nota = nota_dreta;
          return 1 + right;
        }
        else return 1 + left;
      }
      if (left != -1) return 1 + left;
      else {
        nota = nota_dreta;
        return 1 + right;
      }
    }
  }
  return -1;
}

int main() {
  BinTree<Estudiant> a;
  read_bintree_est(a);
  int dni;
  while (cin >> dni) {
    double nota;
    int profunditat = cerca(a, dni, nota);
    if (profunditat == -1) cout << dni << ' ' << -1 << endl;
    else cout << dni << ' ' << profunditat << ' ' << nota << endl;
  }
}
