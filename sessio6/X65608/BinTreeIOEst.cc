#include "BinTreeIOEst.hh"
using namespace std;

void read_bintree_est(BinTree<Estudiant>& a) {
// Pre: a és buit; el canal estandar d’entrada conté una seqüència
// de parells <int, double> que representa un arbre binari d’estudiants
// en preordre, on un parell amb l’int = ‘‘marca’’ representa
// un arbre buit
// Post: a conté l’arbre que hi havia al canal estandar d’entrada
  Estudiant est;
  est.llegir();
  if (est.consultar_DNI() != 0 or (est.te_nota() and est.consultar_nota() != 0)) {
    BinTree<Estudiant> left;
    read_bintree_est(left);
    BinTree<Estudiant> right;
    read_bintree_est(right);
    BinTree<Estudiant> aux(est, left, right);
    a = aux;
  }
}
