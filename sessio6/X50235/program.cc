#include "BinTree.hh"
#include "BinTreeIOint.hh"
using namespace std;

bool pertany(BinTree<int> a, int x) {
  if (a.empty()) return false;
  //else if (a.left().empty() and a.right().empty()) return false;
  else {
    if (a.value() == x or pertany(a.left(), x) or pertany(a.right(), x)) return true;
    else return false;
  }
}

BinTree<int> esborrar(BinTree<int> a, int& x) {
  if (not a.empty()) {
    if (not a.left().empty() and a.left().value() == x) {
      BinTree<int> b;
      a = BinTree<int> (a.value(), b, a.right());
      return a;
    }
    else if(not (a.right().empty()) and a.right().value() == x) {
      BinTree<int> b;
      a = BinTree<int> (a.value(), a.left(), b);
      return a;
    }
    else {
      BinTree<int> l;
      l = esborrar(a.left(), x);
      BinTree<int> r;
      r = esborrar(a.right(), x);
      BinTree<int> aux(a.value(), l, r);
      return aux;
    }
  }
  return a;
}

bool poda_subarbre(BinTree<int> &a, int x) {
  if (pertany(a, x)) {
    if (a.value() == x) a = BinTree<int>();
    else a = esborrar(a, x);
    return true;
  }
  return false;
}
/*
int main() {
  BinTree<int> a;
  read_bintree_int(a, 0);
  int x;
  cin >> x;
  cout << boolalpha << poda_subarbre(a, x) << endl;
}
*/
