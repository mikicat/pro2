#include "Cluster.hh"
using namespace std;


Cluster::Cluster() {}

Cluster::Cluster(string id) {
  arbol_esp = BinTree< pair<string, double> >(make_pair(id, -1));
}

// MODIFICADORES

void Cluster::fusiona(const Cluster& c1, const Cluster& c2, double dist) {
  string name1 = c1.arbol_esp.value().first;
  string name2 = c2.arbol_esp.value().first;
  if (name1 < name2) {
    string name = name1+name2;
    arbol_esp = BinTree< pair<string, double> >(make_pair(name, dist), c1.arbol_esp, c2.arbol_esp);
  }
  else {
    string name = name2+name1;
    arbol_esp = BinTree< pair<string, double> >(make_pair(name, dist), c2.arbol_esp, c1.arbol_esp);
  }
}

// CONSULTORES
string Cluster::consulta_id() const {
  return arbol_esp.value().first;
}

// I/O
void Cluster::i_imprime(const BinTree< pair<string, double> >& a) const {
  if (not a.empty()) {
    cout << '[';
    if(a.value().second == -1) cout << a.value().first; // En caso de que sea una hoja del árbol
    else cout << '(' << a.value().first << ", " << a.value().second << ") ";

    BinTree< pair<string, double> > l = a.left();
    BinTree< pair<string, double> > r = a.right();
    i_imprime(l);
    i_imprime(r);
    cout << ']';
  }
}

void Cluster::imprime() const {
  i_imprime(arbol_esp);
  cout << endl;
}
