/**
  * @mainpage Práctica: Árbol Filogenético

  INICIO:
    - Leer k
    - Bucle de comandos

  COMANDOS:

    - crea_especie
    - obtener_gen
    - distancia
    - elimina_especie
    - existe_especie
    - lee_cjt_especies
    - imprime_cjt_especies
    - tabla_distancias
    - inicializa_clusters
    - ejecuta_paso_wpgma
    - imprime_cluster
    - imprime_arbol_filogenetico
    - fin
*/

/** @file main.cc
    @brief Programa principal de la práctica del árbol filogenético
*/
#ifndef NO_DIAGRAM
#include <iostream>
#include <string>
#include <map>
#include "BinTree.hh"
#endif
#include "Especie.hh"
#include "Cjt_especies.hh"
#include "Cluster.hh"
#include "Cjt_clusters.hh"

using namespace std;


int main()
{
  // Leer k
  Especie::lee_k();

  // Inicializar conjuntos vacíos
  Cjt_especies especies;
  Cjt_clusters clusters;

  string cmd;
  // Bucle de comandos
  while (cin >> cmd and cmd != "fin") {
    if (cmd == "crea_especie") {
      string id;
      cin >> id;
      Especie e;
      e.lee();
      cout << "# crea_especie " << id << ' ' << e.consulta_gen() << endl;
      if (not especies.existe_especie(id)) {
        especies.anade_especie(id, e);
      }
      else cout << "ERROR: La especie " << id << " ya existe." << endl;
    }
    else if (cmd == "obtener_gen") {
      string id;
      cin >> id;
      cout << "# obtener_gen " << id << endl;
      if (especies.existe_especie(id)) {
        Especie e = especies.consulta_especie(id);
        cout << e.consulta_gen() << endl;
      }
      else cout << "ERROR: La especie " << id << " no existe." << endl;
    }
    else if (cmd == "distancia") {
      string id1, id2;
      cin >> id1 >> id2;
      cout << "# distancia " << id1 << ' ' << id2 << endl;
      if (not(especies.existe_especie(id1)) and not(especies.existe_especie(id2))) cout << "ERROR: La especie " << id1 << " y la especie " << id2 << " no existen." << endl;
      else if (not(especies.existe_especie(id1)) and especies.existe_especie(id2)) cout << "ERROR: La especie " << id1 << " no existe." << endl;
      else if (especies.existe_especie(id1) and not(especies.existe_especie(id2))) cout << "ERROR: La especie " << id2 << " no existe." << endl;
      else {
        double dist = especies.consulta_distancia(id1, id2);
        cout << dist << endl;
      }
    }
    else if (cmd == "elimina_especie") {
      string id;
      cin >> id;
      cout << "# elimina_especie " << id << endl;
      if (especies.existe_especie(id)) especies.elimina_especie(id);
      else cout << "ERROR: La especie " << id << " no existe." << endl;
    }
    else if (cmd == "existe_especie") {
      string id;
      cin >> id;
      cout << "# existe_especie " << id << endl;
      if (especies.existe_especie(id)) cout << "SI";
      else cout << "NO";
      cout << endl;
    }
    else if (cmd == "lee_cjt_especies") {
      cout << "# lee_cjt_especies" << endl;
      especies.lee();
    }
    else if (cmd == "imprime_cjt_especies") {
      cout << "# imprime_cjt_especies" << endl;
      especies.imprime();
    }
    else if (cmd == "tabla_distancias") {
      cout << "# tabla_distancias" << endl;
      especies.imprime_tabla_distancias();
    }
    else if (cmd == "inicializa_clusters") {
      cout << "# inicializa_clusters" << endl;
      clusters.inicializa_clusters(especies);
      clusters.imprime_tabla_distancias();
    }
    else if (cmd == "ejecuta_paso_wpgma") {
      cout << "# ejecuta_paso_wpgma" << endl;
      if (clusters.tamano() > 1) {
        clusters.ejecuta_paso_wpgma();
        clusters.imprime_tabla_distancias();
      }
      else cout << "ERROR: num_clusters <= 1" << endl;
    }
    else if (cmd == "imprime_cluster") {
      string id;
      cin >> id;
      cout << "# imprime_cluster " << id << endl;
      if (clusters.existe_cluster(id)) clusters.imprime_cluster(id);
      else cout << "ERROR: El cluster " << id << " no existe." << endl;
    }
    else if (cmd == "imprime_arbol_filogenetico") {
      cout << "# imprime_arbol_filogenetico" << endl;
      clusters.imprime_arbol_filogenetico(especies);

    }
    cout << endl;
  }
}
