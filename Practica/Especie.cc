#include "Especie.hh"
#ifndef NO_DIAGRAM
#include <iostream>
#include <string>
#include <map>
#endif

int Especie::k;

Especie::Especie() {}

void Especie::calcular_kmer() {
  for (int i = 0; i <= gen.size()-k; ++i) {
    ++kmer[gen.substr(i, k)];
  }
}

string Especie::consulta_gen() const {
  return gen;
}

double Especie::distancia(const Especie& e2) const {
  double inter = 0;
  map<string, bool> inter_parts;
  map<string, int>::const_iterator it1 = kmer.begin();
  map<string, int>::const_iterator it2 = e2.kmer.begin();

  while (it1 != kmer.end() and it2 != e2.kmer.end()) { // Calcula la intersección de los conjuntos
    if (it1->first < it2->first) ++it1;
    else if (it1->first == it2->first and not inter_parts[it1->first]) {
      if (it1->second > it2->second) inter += it2->second;
      else inter += it1->second;
      inter_parts[it1->first] = true;
      ++it1;
      ++it2;
    }
    else ++it2;
  }

  double unio = 0;
  for (it1 = kmer.begin(); it1 != kmer.end(); ++it1) { // Calcula la unión de los conjuntos
    if (inter_parts[it1->first]) { // Comprueba si el elemento es parte de la intersección de los dos kmers
      it2 = e2.kmer.find(it1->first);
      if (it1->second < it2->second) unio += it2->second;
      else unio += it1->second;
    }
    else unio += it1->second;
  }
  for (it2 = e2.kmer.begin(); it2 != e2.kmer.end(); ++it2) {
    if (not inter_parts[it2->first]) unio += it2->second; // Solo añade los que no son parte de la intersección, pues los que son parte ya han sido sumados en el bucle anterior
  }
  return (1 - (inter/unio))*100;
}

void Especie::lee_k() {
  cin >> k;
}

void Especie::lee() {
  cin >> gen;
  calcular_kmer();
}

void Especie::imprime() const {
  cout << gen << endl;
}
