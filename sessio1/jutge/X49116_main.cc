#include "../vectorIOint.hh"
#include "X49116.cc"

int main() {
    vector<int> v;
    leer_vector_int(v);
    
    parint res1 = max_min1(v);
    cout << res1.prim << ' '  << res1.seg << endl;
    pair<int, int> res2 = max_min2(v);
    cout << res2.first << ' ' << res2.second << endl;
    int x, y;
    max_min3(v, x, y);
    cout << x << ' ' << y << endl;
}
