#include <vector>
using namespace std;

struct parint {int prim, seg;};

parint max_min1(const vector<int>& v)
/* Pre: v.size()>0 */ 
/* Post: el primer componente del resultado es el valor maximo de v;
    el segundo componente del resultado es el valor minimo de v */
{
    int min = v[0], max = v[0];
    for (int i = 1; i < v.size(); ++i) {
        if (v[i] < min) min = v[i];
        if (v[i] > max) max = v[i];
    }
    parint res;
    res.prim = max, res.seg = min;
    return res;
}

pair<int, int> max_min2(const vector<int>& v)
 /* Pre: v.size()>0 */ 
 /* Post: el primer componente del resultado es el valor maximo de v;
 el segundo componente del resultado es el valor minimo de v */
{
    int min = v[0], max = v[0];
    for (int i = 1; i < v.size(); ++i) {
        if (v[i] < min) min = v[i];
        if (v[i] > max) max = v[i];
    }
    pair<int, int> res;
    res.first = max, res.second = min;
    return res;
}

void max_min3(const vector<int>& v, int& x, int& y)
 /* Pre: v.size()>0 */ 
 /* Post: x es el valor maximo de v;  y es el valor minimo de v */
{
    y = v[0], x = v[0];
    for (int i = 1; i < v.size(); ++i) {
        if (v[i] < y) y = v[i];
        if (v[i] > x) x = v[i];
    }
}
