#include <iostream>
using namespace std;

int main() {
    int n, x;
    cin >> n >> x;
    for (int i = 0; i < n; ++i) {
        int suma = 0;
        int a;
        cin >> a;
        while (a != x) {
            suma += a;
            cin >> a;
        }
        string s;
        getline(cin,s); // Novetat
        cout << "La suma de la secuencia " << i + 1 << " es " << suma << endl;
    }
}
