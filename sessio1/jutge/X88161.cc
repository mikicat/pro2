#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

typedef vector< vector< pair<int, int> > > Matrix;

struct Team {
    int name;
    int points;
    int marcats;
    int rebuts;
};

vector<Team> classify(const Matrix& m) {
    vector<Team> lliga(m.size());
    for (int i = 0; i < m.size(); ++i) {
        lliga[i].name = i + 1, lliga[i].points = 0, lliga[i].marcats = 0, lliga[i].rebuts = 0;
    }

    for (int i = 0; i < m.size(); ++i) {
        for (int j = 0; j < m.size(); ++j) {
            if (i != j) {
                lliga[i].marcats += m[i][j].first;
                lliga[i].rebuts += m[i][j].second;
                lliga[j].marcats += m[i][j].second;
                lliga[j].rebuts += m[i][j].first;

                if (m[i][j].first > m[i][j].second) lliga[i].points += 3;
                else if (m[i][j].first < m[i][j].second) lliga[j].points += 3;
                else lliga[i].points += 1, lliga[j].points += 1;
            }

        }
    }
    return lliga;
}

bool order(Team& a, Team& b) {
    if (a.points == b.points) {
        if (a.marcats - a.rebuts == b.marcats - b.rebuts) return a.name < b.name;
        return (a.marcats - a.rebuts) > (b.marcats - b.rebuts);
    }
    return a.points > b.points;
}

int main() {
    int n;
    cin >> n;
    Matrix m(n, vector< pair<int, int> >(n));
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) cin >> m[i][j].first >> m[i][j].second;
    }
    vector<Team> league = classify(m);
    sort(league.begin(), league.end(), order);

    for (int i = 0; i < n; ++i) cout << league[i].name << ' ' << league[i].points << ' ' << league[i].marcats << ' ' << league[i].rebuts << endl;

}
