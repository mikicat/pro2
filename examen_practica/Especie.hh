/** @file Especie.hh
    @brief Especificación de la clase Especie
*/
#ifndef _ESPECIE_HH_
#define _ESPECIE_HH_
#ifndef NO_DIAGRAM
#include <iostream>
#include <string>
#include <map>
#endif

using namespace std;

/** @class Especie
    @brief Representa una Especie

    Cada especie tiene un gen asignado, así como el conjunto de todas las agrupaciones de longitud k generadas a partir del gen, asignadas en el mapa kmer.
*/
class Especie {
private:
  /** @brief Constante k para calcular el kmer (la constante es común para todas las especies) */
  static int k;
  /** @brief Gen de la especie*/
  string gen;
  /** @brief Mapa del kmer (agrupaciones de k longitud a partir del gen)*/
  map<string, int> kmer;

  // MODIFICADORES
  /** @brief Modificadora. Calcula el kmer de la especie.

    \pre La especie existe
    \post Se ha calculado el kmer y se ha guardado como atributo
  */
  void calcular_kmer();
public:
  // CREADORES
  /** @brief Creadora por defecto.

      Se ejecuta automáticamente al declarar un conjunto de especies.

      \pre <em>cierto</em>
      \post Se ha creado un conjunto de especies vacío
  */
  Especie();

  // CONSULTORES

  /** @brief Consultora. Devuelve el gen asociado a la especie.

    \pre La especie existe y existe un gen asociado a la especie
    \post Devuelve el gen asociado a la especie
  */
  string consulta_gen() const;

  /** @brief Consultora. Dadas dos especies, devuelve la distancia entre las dos.

  La primera especie es el parámetro implícito.

    \pre Las dos especies existen
    \post Devuelve la distancia entre las dos especies
  */
  double distancia(const Especie& e2) const;


  // I/O

  /** @brief  Lee del canal estándar de entrada una constante k >= 1.

    \pre <em>cierto</em>
    \post La constante k ha sido asignada como atributo static int k.

  */
  static void lee_k();

  /** @brief  Lee del canal estándar de entrada una especie.

    \pre <em>cierto</em>
    \post Existe una especie con el gen, asociada a el identificador dado, y cuyo kmer ha sido calculado.

  */
  void lee();

  /** @brief Imprime en el canal estándar de salida el gen de la especie.

    \pre <em>cierto</em>
    \post Se han escrito en el canal estándar de salida el gen de la especie.
  */
  void imprime() const;
};
#endif
