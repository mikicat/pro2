#include "Cjt_especies.hh"

Cjt_especies::Cjt_especies() {}

void Cjt_especies::anade_especie(const string id, const Especie& e) {
  for (map<string, Especie>::iterator it = mesp.begin(); it != mesp.end(); ++it) { // Crea un nuevo elemento en la tabla de distancias
    if (it->first < id) tbl_dist[make_pair(it->first, id)] = e.distancia(it->second);
    else tbl_dist[make_pair(id, it->first)] = e.distancia(it->second);
  }
  mesp[id] = e; // Añade la especie al conjunto
}

void Cjt_especies::elimina_especie(string id) {
  for(map<string, Especie>::iterator it = mesp.begin(); it != mesp.end(); ++it) { // Borra todas las distancias con la especie 'id' de la tabla de distancias
    if (it->first != id) {
      if (it->first < id) tbl_dist.erase(tbl_dist.find(make_pair(it->first, id)));
      else tbl_dist.erase(tbl_dist.find(make_pair(id, it->first)));
    }
  }
  mesp.erase(id); // Borra la especie 'id' del conjunto de especies
}

bool Cjt_especies::existe_especie(string id) const {
  return (mesp.find(id) != mesp.end());
}

Especie Cjt_especies::consulta_especie(string id) const {
  return mesp.find(id)->second;
}

void Cjt_especies::consulta_iessim_tbl(pair<string, string>& id, double& dist) {
  if (not set_it_tbl or it_tbl == tbl_dist.end()) { // Inicializa el apuntador
    it_tbl = tbl_dist.begin();
    set_it_tbl = true;
  }
  id = it_tbl->first;
  dist = it_tbl->second;
  ++it_tbl;
}

void Cjt_especies::consulta_iessim_cesp(string& id) {
  if (not set_it_mesp or it_mesp == mesp.end()) { // Inicializa el apuntador
    it_mesp = mesp.begin();
    set_it_mesp = true;
  }
  id = it_mesp->first;
  ++it_mesp;
}

double Cjt_especies::consulta_distancia(string id1, string id2) const {
  if (id1 < id2) return tbl_dist.find(make_pair(id1, id2))->second;
  else if (id1 > id2) return tbl_dist.find(make_pair(id2, id1))->second;
  else return 0;
}

int Cjt_especies::tamano() const {
  return mesp.size();
}

int Cjt_especies::tbl_size() const {
  return tbl_dist.size();
}

void Cjt_especies::imprime_tabla_distancias() const {
  string id_fila;
  bool init_row;
  for (Tbl::const_iterator it = tbl_dist.begin(); it != tbl_dist.end(); ++it) {
    if (it == tbl_dist.begin() or it->first.first != id_fila) {
      id_fila = it->first.first;
      init_row = true;
    }
    if (init_row) {
      init_row = false;
      if (it != tbl_dist.begin()) cout << endl;
      cout << it->first.first << ':';
    }
    cout << ' ' << it->first.second << " (" << it->second << ")";
  }
  // Último elemento (vacío):
  if (mesp.begin() != mesp.end()) {
    if (mesp.size() > 1) cout << endl;
    map<string, Especie>::const_iterator ultim = mesp.end();
    --ultim;
    cout << ultim->first << ':' << endl;
  }
}

void Cjt_especies::lee() {
  mesp.clear();
  tbl_dist.clear();
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    string id;
    cin >> id;
    Especie e;
    e.lee();
    anade_especie(id, e);
  }
}

void Cjt_especies::imprime() const {
  for (map<string, Especie>::const_iterator it = mesp.begin(); it != mesp.end(); ++it) {
    cout << it->first << ' ';
    it->second.imprime(); // Imprime el gen de la especie
  }
}
