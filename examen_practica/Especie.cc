#include "Especie.hh"
#ifndef NO_DIAGRAM
#include <iostream>
#include <string>
#include <map>
#include <cmath>
#endif

int Especie::k;

Especie::Especie() {}

void Especie::calcular_kmer() {
  for (int i = 0; i <= gen.size()-k; ++i) {
    ++kmer[gen.substr(i, k)];
  }
}

string Especie::consulta_gen() const {
  return gen;
}

double Especie::distancia(const Especie& e2) const {
  double num = 0;
  double den1 = 0;
  double den2 = 0;
  map<string, int>::const_iterator it1 = kmer.begin();
  map<string, int>::const_iterator it2 = e2.kmer.begin();

  while (it1 != kmer.end() and it2 != e2.kmer.end()) { // Calcula la intersección de los conjuntos
    if (it1->first < it2->first) {
      num += it1->second * it1->second;
      den1 += it1->second * it1->second;
      ++it1;
    }
    else if (it1->first == it2->first) {
      num += (it1->second - it2->second) * (it1->second - it2->second);
      den1 += it1->second * it1->second;
      den2 += it2->second * it2->second;
      ++it1;
      ++it2;
    }
    else {
      num += it2->second * it2->second;
      den2 += it2->second * it2->second;
      ++it2;
    };
  }
  while (it1 != this->kmer.end()) {
    num += (it1->second)*(it1->second);
    den1 += it1->second * it1->second;
    ++it1;
  }
  while (it2 != e2.kmer.end()) {
    num += (it2->second)*(it2->second);
    den2 += it2->second * it2->second;
    ++it2;
  }
  if (den1 + den2 == 0) return 100;
  else {
    return (1 - (sqrt(num)/(sqrt(den1) + sqrt(den2))))*100;
  }

}

void Especie::lee_k() {
  cin >> k;
}

void Especie::lee() {
  cin >> gen;
  calcular_kmer();
}

void Especie::imprime() const {
  cout << gen << endl;
}
