/** @file Cjt_especies.hh
    @brief Especificación de la clase Conjunto de especies
*/
#ifndef _CJTESP_HH_
#define _CJTESP_HH_
#ifndef NO_DIAGRAM
#include <iostream>
#include <string>
#include <map>
#endif
#include "Especie.hh"

using namespace std;



/** @class Cjt_especies
    @brief Representa un Conjunto de especies

    Cada especie tiene un identificador y un gen asignado.
*/
class Cjt_especies {

private:
  /** @brief Definición de un tipo Tbl (Tabla de distancias) */
  typedef map< pair<string, string>, double> Tbl;
  /** @brief Mapa del conjunto de especies */
  map<string, Especie> mesp;
  /** @brief Tabla de distancias */
  Tbl tbl_dist;

  /** @brief Iterador de la tabla de distancias */
  Tbl::iterator it_tbl;
  /** @brief Iterador del mapa del conjunto de especies */
  map<string, Especie>::iterator it_mesp;

  /** @brief Booleanos de comprovación de las inicializaciones de los iteradores */
  bool set_it_tbl = false;
  bool set_it_mesp = false;

public:
  /** @brief Creadora por defecto.

      Se ejecuta automáticamente al declarar un conjunto de especies.

      \pre <em>cierto</em>
      \post Se ha creado un conjunto de especies vacío
  */
  Cjt_especies();


  // MODIFICADORES

  /** @brief Crea una especie con el gen y se asocia a un identificador.

  Escribe un mensaje de error si ya existe una especie con el mismo identificador.
  La especie creada, si no hay error, se agrega al conjunto de especies.

    \pre la especie no existe
    \post existe una nueva especie que ha sido añadida al conjunto de especies.

  */
  void anade_especie(const string id, const Especie& e);

  /** @brief Dado el identificador de una especie e la elimina del conjunto de especies.

  Escribe un mensaje de error si la especie con el identificador dado no existe.

    \pre La especie e existe
    \post La especie e ha sido eliminada del conjunto de especies

  */
  void elimina_especie(string id);


  // CONSULTORES

  /** @brief Consultora. Dado el identificador de una especie e imprime una indicación de si dicha especie existe (es decir, es parte del conjunto de especies).
    \pre <em>cierto</em>
    \post Indica si dicha especie existe
  */
  bool existe_especie(string id) const;

  /** @brief Consultora. Dado un identificador, devuelve la especie.

  Se escribe un mensaje de error si alguna de las dos especies cuyos identificadores se dan no existen

    \pre La especie existe
    \post Devuelve la especie asociada al identificador id.
  */
  Especie consulta_especie(string id) const;

  /** @brief Consultora. Asigna el identificador y la distancia relativa entre dos especies de la tabla a las variables que se pasan por referencia según el iterador it_tbl

    \pre it_tbl no inicializado o tbl_dist.begin() <= it_tbl <= tbl_dist.end()
    \post it_tbl se ha incrementado en 1 posición, id tiene el identificador y dist la distancia relativa entre dos especies de la tabla

  */
  void consulta_iessim_tbl(pair<string, string>& id, double& dist);

  /** @brief Consultora. Asigna el identificador de la especie a la que hace referencia it_mesp a la variable id

    \pre it_mesp no inicializado o mesp.begin() <= it_mesp <= mesp.end()
    \post it_mesp se ha incrementado en 1 posición, id tiene el identificador de la especie a la que it_mesp hace referencia.

  */
  void consulta_iessim_cesp(string& id);

  /** @brief Consultora. Devuelve la distancia entre un par de especies del conjunto de especies.

    \pre Las dos especies existen
    \post Se ha devuelto la distancia entre las dos especies.
  */
  double consulta_distancia(string id1, string id2) const;

  /** @brief Consultora. Devuelve el tamaño del conjunto de especies.

    \pre <em>cierto</em>
    \post Devuelve el tamaño del conjunto de especies.
  */
  int tamano() const;

  /** @brief Consultora. Devuelve el tamaño de la tabla del conjunto de especies.

    \pre <em>cierto</em>
    \post Devuelve el tamaño de la tabla del conjunto de especies.
  */
  int tbl_size() const;


  // ESCRIPTURA

  /** @brief Imprime la tabla de distancias entre cada par de especies del conjunto de especies. Si el conjunto es vacío, no imprime ninguna información.
    \pre <em>cierto</em>
    \post Se ha escrito la tabla de distancias por el canal estándar de salida.
  */
  void imprime_tabla_distancias() const;

  /** @brief  Lee del canal estándar de entrada un entero n ≥ 0 y a continuación una secuencia de n especies (pares identificador-gen).

  Las n especies dadas tienen identificadores distintos entre sí. Los contenidos previos del conjunto de especies se descartan
  —las especies dejan de existir— y las n especies leídas se agregan al conjunto de especies.

    \pre <em>cierto</em>
    \post El conjunto de especies tiene las n especies.

  */
  void lee();

  /** @brief Imprime en el canal estándar de salida el conjunto de especies. Si el conjunto es vacío, no imprime ninguna información.

    \pre <em>cierto</em>
    \post Se han escrito en el canal estándar de salida el conjunto de especies.
  */
  void imprime() const;
};
#endif
