#include "Cluster.hh"
using namespace std;


Cluster::Cluster() {}

Cluster::Cluster(string id) {
  root arrel;
  arrel.first = id;
  arrel.second = -1;
  arrel.hojas = 1;
  arbol_esp = BinTree< root >(arrel);
}

// MODIFICADORES

void Cluster::fusiona(const Cluster& c1, const Cluster& c2, double dist) {
  string name1 = c1.arbol_esp.value().first;
  string name2 = c2.arbol_esp.value().first;
  if (name1 < name2) {
    string name = name1+name2;
    root arrel;
    arrel.first = name;
    arrel.second = dist;
    arrel.hojas = c1.arbol_esp.value().hojas + c2.arbol_esp.value().hojas;
    arbol_esp = BinTree< root >(arrel, c1.arbol_esp, c2.arbol_esp);
  }
  else {
    string name = name2+name1;
    root arrel;
    arrel.first = name;
    arrel.second = dist;
    arrel.hojas = c1.arbol_esp.value().hojas + c2.arbol_esp.value().hojas;
    arbol_esp = BinTree< root >(arrel, c2.arbol_esp, c1.arbol_esp);
  }
}

// CONSULTORES
string Cluster::consulta_id() const {
  return arbol_esp.value().first;
}

int Cluster::consulta_hojas() const {
  return arbol_esp.value().hojas;
}

// I/O
void Cluster::i_imprime(const BinTree< root >& a) const {
  if (not a.empty()) {
    cout << '[';
    if(a.value().second == -1) cout << a.value().first; // En caso de que sea una hoja del árbol
    else cout << '(' << a.value().first << ", " << a.value().second << ") ";

    BinTree< root > l = a.left();
    BinTree< root > r = a.right();
    i_imprime(l);
    i_imprime(r);
    cout << ']';
  }
}

void Cluster::imprime() const {
  i_imprime(arbol_esp);
  cout << endl;
}
