/** @file Cjt_clusters.hh
    @brief Especificación de la clase Conjunto de clústers
*/
#ifndef _CJTCLU_HH_
#define _CJTCLU_HH_
#include "Cluster.hh"
#include "Cjt_especies.hh"
#ifndef NO_DIAGRAM
#include <iostream>
#include <string>
#include <map>
#include "BinTree.hh"
#endif

/** @class Cjt_clusters
    @brief Representa un Conjunto de clusters
*/
class Cjt_clusters {
private:
  /** @brief Definición de un tipo Tbl (Tabla de distancias) */
  typedef map< pair<string, string>, double> Tbl;
  /** @brief Mapa del conjunto de clusters */
  map<string, Cluster> cclust;
  /** @brief Tabla de distancias */
  Tbl tbl_dist;

  /** @brief Actualiza la tabla de distancias entre clusters

    \pre c es el nuevo cluster, s1 y s2 son los identificadores de los clusters fusionados.
    \post Las distancias con el nuevo clúster han sido añadidas a la tabla. Las distancias entre cada cluster previo (de los dos fusionados) con los otros clusters ha sido eliminada de la tabla.
  */
  void actualiza_tabla(const Cluster& c, string s1, string s2);

  /** @brief Devuelve la distancia entre dos clusters pasados por identificadores.
    \pre s1 y s2 son los identificadores de dos clusters que existen.
    \post Se ha devuelto la distancia entre los dos clusters.
  */
  double dist(string s1, string s2) const;

public:

  // CREADORES
  /** @brief Creadora por defecto.

  Se ejecuta automáticamente al declarar un conjunto de clusters.

    \pre <em>cierto</em>
    \post Se ha creado un conjunto de clusters vacío.
  */
  Cjt_clusters();

  // MODIFICADORES

  /** @brief Inicializa el conjunto de clústers con el conjunto de especies en el estado en el que esté en ese momento.

    \pre <em>cierto</em>
    \post Se ha copiado la tabla de distancias entre especies a la tabla de distancias entre clusters.

  */
  void inicializa_clusters(Cjt_especies& cesp);

  /** @brief ejecuta un paso del algoritmo WPGMA (fusiona los dos clústers a menor distancia en uno nuevo) .

    \pre Existe más de un cluster en el conjunto
    \post Se ha ejecutado un paso del algoritmo WPGMA.

  */
  void ejecuta_paso_clust();

  // CONSULTORES

  /** @brief Consultora. Dado el identificador de un clúster c imprime una indicación de si dicho clúster existe (es decir, es parte del conjunto de clústers).

    \pre <em>cierto</em>
    \post Indica si dicho clúster existe.
  */
  bool existe_cluster(string id) const;

  /** @brief Consultora. Devuelve el tamaño del conjunto de clústers.

    \pre <em>cierto</em>
    \post Devuelve el tamaño del conjunto de clústers.
  */
  int tamano() const;




  // ESCRIPTURA

  /** @brief Imprime la tabla de distancias entre clusters.

   \pre <em>cierto</em>
   \post Se ha escrito la tabla de distancias entre clusters.
  */
  void imprime_tabla_distancias() const;


  /** @brief dado un identificador α, imprime el clúster (su “estructura arborescente”) con el identificador dado.

   \pre El clúster existe
   \post Se ha escrito la estructura arborescente del clúster con el identificador dado.
  */
  void imprime_cluster(string id) const;

  /** @brief imprime el árbol filogenético para el conjunto de especies actual

  Dicho árbol es el clúster que agrupa todas las especies, resultante de aplicar
  el algoritmo WPGMA. El contenido del conjunto de clústers previo se descarta y se reinicializa
  con el conjunto de especies en el estado en el que esté en ese momento, para za continuación
  aplicar el algoritmo. El conjunto de clústers final es el que queda despuésde aplicar el algoritmo.
  Se imprimirá la estructura arborescente del clúster con los identificadores de los clústers
  (raíces de los subárboles) y la distancia entre cada clúster y sus hojas descendientes
  Si el nuevo conjunto de clústers es vacío, solamente se escribe un mensaje de error.

    \pre <em>cierto</em>
    \post Se ha escrito el árbol filogenético por el canal estándar de salida, o si el conjunto es vacío, se ha escrito un error.
  */
  void imprime_arbol_filogenetico(Cjt_especies& cesp);

};
#endif
