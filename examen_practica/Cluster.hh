/** @file Cluster.hh
    @brief Especificación de la clase Cluster
*/
#ifndef _CLUSTER_HH_
#define _CLUSTER_HH_
#ifndef NO_DIAGRAM
#include <iostream>
#include <string>
#include "BinTree.hh"
#endif

struct root {
  string first;
  double second;
  int hojas;
};

/** @class Cluster
    @brief Representa un Cluster
*/
class Cluster {
private:
  /** @brief Árbol binario del Clúster */
  BinTree<root> arbol_esp;

  /** @brief Función de inmersión para la función imprime()
    \pre El clúster "a" existe
    \post Se ha escrito la estructura arborescente del clúster "a"
  */
  void i_imprime(const BinTree< root >& a) const;

public:
  // CREADORES
  /** @brief Creadora por defecto.

  Se ejecuta automáticamente al declarar un cluster.

    \pre <em>cierto</em>
    \post Se ha creado un cluster vacío.
  */
  Cluster();

  /** @brief Creadora por identificador.

  Se ejecuta al declarar un cluster con identificador.

    \pre <em>cierto</em>
    \post Se ha creado un cluster con un árbol que contiene una sola raíz.
  */
  Cluster(string id);

  // MODIFICADORES

  /** @brief Fusiona dos clústers en uno de nuevo (el parámetro implícito)

  Se ejecuta al ejecutar un paso del algoritmo wpgma

    \pre <em>cierto</em>
    \post Se ha creado un clúster con los dos clústers anteriores.
  */
  void fusiona(const Cluster& c1, const Cluster& c2, double dist);

  // CONSULTORES
  /** @brief Consulta el identificador asociado a la raíz del clúster

    \pre El clúster existe
    \post Se ha devuelto el identificador
  */
  string consulta_id() const;

  int consulta_hojas() const;

  // I/O

  /** @brief imprime el clúster (su “estructura arborescente”)

   \pre El clúster existe
   \post Se ha escrito la estructura arborescente del clúster.
  */
  void imprime() const;
};
#endif
