#include "Cjt_clusters.hh"
using namespace std;

// CREADORES

Cjt_clusters::Cjt_clusters() {}

// MODIFICADORES

void Cjt_clusters::inicializa_clusters(Cjt_especies& cesp) {
  cclust.clear();
  tbl_dist.clear();

  for (int i = 0; i < cesp.tbl_size(); ++i) { // Genera la tabla de distancias
    pair<string, string> id;
    double dist;
    cesp.consulta_iessim_tbl(id, dist);
    tbl_dist[id] = dist;
  }

  for (int i = 0; i < cesp.tamano(); ++i) { // Genera los nuevos clusters
    string id;
    cesp.consulta_iessim_cesp(id);
    Cluster c(id);
    cclust[id] = c;
  }
}

void Cjt_clusters::ejecuta_paso_clust() {
  double mindist = -1;
  Tbl::iterator it = tbl_dist.begin();
  Tbl::iterator min_it;
  while (it != tbl_dist.end() and mindist != 0) {
    double d = it->second;
    if (mindist < 0 or d < mindist) {
      min_it = it;
      mindist = d;
    }
    ++it;
  }

  Cluster nuevo_cluster;
  nuevo_cluster.fusiona(cclust[min_it->first.first], cclust[min_it->first.second], mindist/2); // Nuevo cluster -> fusión de los dos a mínima distancia

  actualiza_tabla(nuevo_cluster, min_it->first.first, min_it->first.second); // Actualiza la tabla de distancias
  cclust[nuevo_cluster.consulta_id()] = nuevo_cluster; // Añade el nuevo cluster al conjunto
  // Borra los clusters que se han fusionado del conjunto y su distancia entre ellos de la tabla de distancias
  cclust.erase(cclust.find(min_it->first.first));
  cclust.erase(cclust.find(min_it->first.second));
  tbl_dist.erase(tbl_dist.find(min_it->first));
}

void Cjt_clusters::actualiza_tabla(const Cluster& c, string s1, string s2) {
  map<string, Cluster>::iterator it = cclust.begin();
  while (it != cclust.end()) {
    if (it->first != s1 and it->first != s2) {
      int hojas1 = cclust[s1].consulta_hojas();
      int hojas2 = cclust[s2].consulta_hojas();
      double new_dist = (hojas1 * dist(it->first, s1) + hojas2 * dist(it->first, s2))/ (hojas1 + hojas2);
      if (it->first < c.consulta_id()) tbl_dist[make_pair(it->first, c.consulta_id())] = new_dist; // Añade a la tabla la distancia entre el nuevo clúster y los anteriores
      else tbl_dist[make_pair(c.consulta_id(), it->first)] = new_dist;

      if (it->first < s1) tbl_dist.erase(tbl_dist.find(make_pair(it->first, s1)));
      else tbl_dist.erase(tbl_dist.find(make_pair(s1, it->first)));

      if (it->first < s2) tbl_dist.erase(tbl_dist.find(make_pair(it->first, s2)));
      else tbl_dist.erase(tbl_dist.find(make_pair(s2, it->first)));
    }
    ++it;
  }
}

void Cjt_clusters::imprime_tabla_distancias() const {
  string id_fila;
  bool init_row;
  for (Tbl::const_iterator it = tbl_dist.begin(); it != tbl_dist.end(); ++it) {
    if (it == tbl_dist.begin() or it->first.first != id_fila) {
      id_fila = it->first.first;
      init_row = true;
    }
    if (init_row) {
      init_row = false;
      if (it != tbl_dist.begin()) cout << endl;
      cout << it->first.first << ':';
    }
    cout << ' ' << it->first.second << " (" << it->second << ")";
  }
  // Último elemento (vacío):
  if (cclust.begin() != cclust.end()) {
    if (cclust.size() > 1) cout << endl;
    map<string, Cluster>::const_iterator ultim = cclust.end();
    --ultim;
    cout << ultim->first << ':' << endl;
  }
}

// CONSULTORES

double Cjt_clusters::dist(string s1, string s2) const {
  if (s1 < s2) return tbl_dist.find(make_pair(s1, s2))->second;
  else return tbl_dist.find(make_pair(s2, s1))->second;
}

bool Cjt_clusters::existe_cluster(string id) const {
  return cclust.find(id) != cclust.end();
}

int Cjt_clusters::tamano() const {
  return cclust.size();
}

// ESCRIPTURA

void Cjt_clusters::imprime_cluster(string id) const {
  cclust.find(id)->second.imprime();
}

void Cjt_clusters::imprime_arbol_filogenetico(Cjt_especies& cesp) {
  inicializa_clusters(cesp);
  while(cclust.size() > 1) {
    ejecuta_paso_clust();
  }
  if (cclust.size() > 0) cclust.begin()->second.imprime();
  else cout << "ERROR: El conjunto de clusters es vacio." << endl;
}
