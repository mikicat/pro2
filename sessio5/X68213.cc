#include <iostream>
#include <string>
#include <stack>
#include <vector>
using namespace std;

void print_stack(stack<string> categoria) {
  while (not categoria.empty()){
    cout << categoria.top() << endl;
    categoria.pop();
  }
  cout << endl;
}

int main() {
  int n;
  cin >> n;
  vector< stack<string> > cataleg(n);

  int x;
  cin >> x;
  while (x != -4) {
    int cat;
    if (x == -1) {
      string title;
      cin >> title >> cat;
      cataleg[cat - 1].push(title);
    }
    else if (x == -2) {
      cin >> n >> cat;
      for (int i = 0; i < n; ++i) cataleg[cat - 1].pop();
    }
    else if (x == -3) {
      int cat;
      cin >> cat;
      cout << "Pila de la categoria " << cat << endl;
      print_stack(cataleg[cat - 1]);

    }
    cin >> x;

  }
}
