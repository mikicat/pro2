#include "CuaIOParInt.hh"
#include <queue>
using namespace std;

void distribuir(queue<ParInt>& c, queue<ParInt>& c1, queue<ParInt>& c2) {
  int time1 = 0, time2 = 0;
  while (not c.empty()) {
    if (time1 > time2) {
      c2.push(c.front());
      time2 += c.front().segon();
    }
    else {
      c1.push(c.front());
      time1 += c.front().segon();
    }
    c.pop();
  }
}

int main() {
  queue<ParInt> c;
  llegirCuaParInt(c);
  queue<ParInt> c1, c2;

  distribuir(c, c1, c2);

  escriureCuaParInt(c1);
  cout << endl;
  escriureCuaParInt(c2);
}
