#include <iostream>
#include <queue>
using namespace std;

void recalc(queue<int> cua, int& min, int& max, double& av) {
  av -= cua.front();
  if (cua.size() == 1) min = 1001, max = -1001;
  else if (cua.size() > 1){
    if (cua.front() == max or cua.front() == min) {
      min = 1001, max = -1001;
      cua.pop();
      while (not cua.empty()){
        if (cua.front() < min) min = cua.front();
        if (cua.front() > max) max = cua.front();
        cua.pop();
      }
    }
  }
}

void afegir(queue<int>& cua, int& min, int& max, double& av, const int n) {
  if (n > max) max = n;
  else if (n < min) min = n;
  av += n;
  cua.push(n);
}

int main() {
  queue<int> cua;
  int n;
  cin >> n;
  double av = 0;
  int min = 1001, max = -1001;
  while (n != 1001) {
    if (n == -1001) {
      if (not cua.empty()) {
        recalc(cua, min, max, av);
        cua.pop();
      }
    }
    else afegir(cua, min, max, av, n);

    if (not cua.empty()) {
      cout << "min " << min << "; max " << max << "; media " << av/cua.size() << endl;
    }
    else cout << 0 << endl;

    cin >> n;
  }
}
