#include <iostream>
#include <stack>
using namespace std;

bool eval() {
  stack<char> open;
  char c;
  cin >> c;
  while (c != '.') {
    if (c == '[' or c == '(') open.push(c);
    else {
      if (not open.empty()) {
        if (c == ']' and open.top() != '[') return false;
        open.pop();
      }
      else return false;
    }

    cin >> c;
  }
  return open.empty();
}

int main() {
  if (eval()) cout << "Correcte";
  else cout << "Incorrecte";
  cout << endl;
}
