#include <iostream>
#include <stack>
using namespace std;

int main() {
  int n;
  cin >> n;
  stack<int> chain;
  int x;
  for (int i = 0; i < n/2; ++i) {
    cin >> x;
    chain.push(x);
  }
  if (n%2 != 0) cin >> x;

  int i = 0;
  bool same = true;
  while (i < n/2 and same) {
    cin >> x;
    if (x != chain.top()) same = false;
    chain.pop();
    ++i;
  }

  if (same) cout << "SI";
  else cout << "NO";
  cout << endl;
}
