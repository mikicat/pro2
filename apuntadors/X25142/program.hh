T i_max_suma_cami(node_arbreGen* node) const {
  if (node == nullptr) return 0;
  else {
    T sum = 0;
    for (int i = 0; i < node->seg.size(); ++i) {
      T sum_anterior = i_max_suma_cami(node->seg[i]);
      if (sum_anterior > sum) sum = sum_anterior;
    }
    return node->info + sum;

  }
}
T max_suma_cami() const
/* Pre: el parametre implicit no es buit */
/* Post: el resultat es la suma del cami de suma maxima del parametre implicit */
{
  return i_max_suma_cami(primer_node);
}
