void i_buscar(node_arbreGen* node, const T& x, bool& found) const {
  if (node == nullptr) found = false;
  else {
    if (node->info == x) found = true;
    int i = 0;
    while(i < node->seg.size() and not found) {
      i_buscar(node->seg[i], x, found);
      ++i;
    }
  }
}

bool buscar(const T& x) const
/* Pre: cert */
/* Post: el resultat indica si x es troba al p.i. o no */
{
  bool found = false;
  i_buscar(primer_node, x, found);
  return found;
}
