void i_arbsuma(node_arbreNari* node, node_arbreNari* &node_suma, int& suma) const
{
  if (node == nullptr) suma = 0;
  else {
    suma = node->info;
    node_suma = new node_arbreNari;
    node_suma->seg = vector<node_arbreNari*>(node->seg.size(), nullptr);
    for (int i = 0; i < node->seg.size(); ++i) {
      int sum_interna;
      node_arbreNari *n = nullptr;
      i_arbsuma(node->seg[i], n, sum_interna);

      suma += sum_interna;
      node_suma->seg[i] = n;
    }
    node_suma->info = suma;
  }
}

void arbsuma(ArbreNari& asum) const {
  int suma;
  i_arbsuma(primer_node, asum.primer_node, suma);
}
