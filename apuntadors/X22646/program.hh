void trenat(Cua &c)
 /* Pre: p.i. = C1, c = C2 */
 /* Post: el p.i. passa a ser el resultat de trenar C1 i C2; c passa a ser buida */
 {
   if (longitud == 0) {
     primer_node = c.primer_node;
     ultim_node = c.ultim_node;
   }
   else
   {
     node_cua *p = primer_node;
     node_cua *s = c.primer_node;

     while (p != nullptr and s != nullptr) {
       node_cua *aux = p->seguent;
       p->seguent = new node_cua;
       (p->seguent)->info = s->info;
       (p->seguent)->seguent = aux;

       if (p == ultim_node) ultim_node = p->seguent;
       p = aux;
       s = s->seguent;
     }
     if (s != nullptr) {
       ultim_node->seguent = s;
       ultim_node = c.ultim_node;
     }
   }

   longitud += c.longitud;
   c.primer_node = nullptr;
   c.ultim_node = nullptr;
   c.longitud = 0;
 }
