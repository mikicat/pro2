void i_max(node_arbreNari* node, T& max) const
{
  if (node != nullptr) {
    if (node->info > max) max = node->info;
    for(int i = 0; i < node->seg.size(); ++i) {
      i_max(node->seg[i], max);
    }
  }
}
T maxim() const
/* Pre: el p.i. no és buit */
/* Post: el resultat indica el valor més gran que conté el p.i. */
{
  T max = primer_node->info;
  i_max(primer_node, max);
  return max;
}
