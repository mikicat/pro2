Llista  reorganitzar_out(const T& x) const
/* Pre: cert */
/* Post: el resultat és una llista que conté els mateixos
elements que el p.i. tal que tots els més petits o iguals
que x al p.i. precedeixen als més grans que x al p.i. però
sempre respectant l'ordre que hi havia entre ells al p.i.
L'element actual de la llista retornada és el primer dels
més grans que x, si existeix, sinó es situa a la detra del
tot. El p.i. no es modifica */
{
  Llista org;
  if (primer_node == nullptr) {
    return org;
  }
  else {
    org.longitud = longitud;
    node_llista *m = primer_node;

    org.primer_node = org.ultim_node = org.act = new node_llista;
    org.act->info = m->info;

    m = m->seg;
    while (m != nullptr) {
      if (m->info > x) {
        org.ultim_node->seg = new node_llista;
        (org.ultim_node->seg)->ant = org.ultim_node;
        org.ultim_node = org.ultim_node->seg;
        org.ultim_node->info = m->info;
        if (org.act->info <= x) org.act = org.ultim_node;
      }
      else {
        if (org.primer_node->info > x) {
          org.primer_node->ant = new node_llista;
          (org.primer_node->ant)->seg = org.primer_node;
          (org.primer_node->ant)->info = m->info;
          org.primer_node = org.primer_node->ant;
        }
        else {
          node_llista *n = new node_llista;
          n->info = m->info;
          if (org.act->info <= x) {
            n->ant = org.act;
            org.act->seg = n;
            org.act = n;
            org.ultim_node = n;
          }
          else {
            n->seg = org.act;
            n->ant = org.act->ant;
            (org.act->ant)->seg = n;
            org.act->ant = n;
          }
        }
      }
      m = m->seg;
    }
    if (org.act->info <= x) org.act = nullptr;
  }
  return org;
}
