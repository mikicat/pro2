#include <iostream>
#include "LlistaIOint.hh"
using namespace std;

int main() {
  Llista<int> l;
  llegir_llista_int(l, -1);
  int x = 5;
  Llista<int> nova = l.reorganitzar_out(x);
  escriure_llista_int(nova);

}
