void esborrar_tots(const T& x)
/* Pre: parametre implicit = P */
/* Post: s'han eliminat del parametre implicit totes les aparicions d'x (la
   resta d'elements queda en el mateix ordre que a P); si el punt d'interes de P
   referenciava a una aparicio d'x, passa a referenciar al primer element
   diferent d'x posterior a aquesta (si no hi ha cap element diferent d'x, passa
   a la dreta el tot); en cas contrari, el punt d'interes no canvia */
{
  node_llista *n = primer_node;
  while (n != nullptr) {
    node_llista *aux = n->seg;
    if (n->info == x) {
      if (n == act) act = n->seg;

      if (n == primer_node) primer_node = n->seg;
      else (n->ant)->seg = n->seg;

      if (n == ultim_node) ultim_node = n->ant;
      else (n->seg)->ant = n->ant;
      
      delete n;
      longitud--;
    }
    n = aux;
  }
}
