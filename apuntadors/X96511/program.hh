void i_freq(node_arbreGen* node, const T& x, int& f) const
{
  if (node != nullptr) {
    if (node->info == x) ++f;
    for (int i = 0; i < node->seg.size(); ++i) {
      i_freq(node->seg[i], x, f);
    }
  }
}

int freq(const T& x) const
/* Pre: cert */
/* Post: el resultat indica el nombre d'aparicions de x en el p.i. */
{
  int f = 0;
  i_freq(primer_node, x, f);
  return f;
}
