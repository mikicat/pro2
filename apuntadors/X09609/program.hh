void i_sub_arrel(node_arbre* node, node_arbre* &asub_node, const T& x, int prof, int& min_prof)
{
  if (node != nullptr) {
    if (node->info == x and (prof <= min_prof or min_prof == -1)) {
      asub_node = copia_node_arbre(node);
      min_prof = prof;
    }
    i_sub_arrel(node->segE, asub_node, x, ++prof, min_prof);
    i_sub_arrel(node->segD, asub_node, x, ++prof, min_prof);
  }
}
void sub_arrel(Arbre& asub, const T& x)
 /* Pre: p.i. = A, asub es buit */
 /* Post: si A conte x, asub es el subarbre d'A resultat de la cerca;
    si A no conte x, asub es buit */
{
  int min_prof = -1;
  i_sub_arrel(primer_node, asub.primer_node, x, 0, min_prof);
}
