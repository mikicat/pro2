void i_arb_sumes(node_arbre* node, node_arbre* &node_suma, int& suma) const
{
  if (node == nullptr) suma = 0;
  else {
    suma = node->info;
    int sum_l, sum_r;
    node_arbre *n_l, *n_r;
    n_l = n_r = nullptr;
    i_arb_sumes(node->segE, n_l, sum_l);
    i_arb_sumes(node->segD, n_r, sum_r);

    suma += sum_l + sum_r;

    node_suma = new node_arbre;
    node_suma->info = suma;
    node_suma->segE = n_l;
    node_suma->segD = n_r;
  }
}

void arb_sumes(Arbre<int> &asum) const
/* Pre: cert */
/* Post: l'arbre asum és l'arbre suma del p.i. */
{
  int suma;
  node_arbre* primer = nullptr;
  i_arb_sumes(primer_node, primer, suma);
  asum.primer_node = primer;
}
