#include <iostream>
#include <set>
using namespace std;

int main() {
  set<string> tots, cap, falta;
  string x;
  while (cin >> x and x != ".") tots.insert(x);
  cap = tots;

  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    falta = tots;
    while (cin >> x and x != ".") {
      cap.erase(x);
      falta.erase(x);
    }
    for (set<string>::iterator it = falta.begin(); it != falta.end(); ++it) {
      tots.erase(*it);
    }
  }

  cout << "Totes les activitats:";
  for (set<string>::iterator it = tots.begin(); it != tots.end(); ++it) {
    cout << ' ' << *it;
  }
  cout << endl;
  cout << "Cap activitat:";
  for(set<string>::iterator it = cap.begin(); it != cap.end(); ++it) {
    cout << ' ' << *it;
  }
  cout << endl;
}
