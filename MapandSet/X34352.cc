#include <iostream>
#include <map>
using namespace std;

int main() {
  map<string, int> diccionari;
  char op;
  while (cin >> op) {
    string partit;
    cin >> partit;
    if (op == 'a') diccionari[partit]++;
    else {
      if (diccionari[partit]) cout << diccionari[partit] << endl;
      else cout << 0 << endl;
    }
  }
}
