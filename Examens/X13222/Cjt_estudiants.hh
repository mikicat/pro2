﻿#ifndef CONJ_EST_HH
#define CONJ_EST_HH

#include "Estudiant.hh"


class Cjt_estudiants {

private:
 
  // Tipus de modul: dades 
 
  // Descripció del tipus: Representa un conjunt d'estudiants
  // matriculats en una assignatura ordenats per DNI. Es poden
  // consultar i modificar els seus elements (de tipus Estudiant)
  // donat un DNI o per posició en l'ordre. També especifica els
  // exàmens parcials que corresponen a la part de laboratori de
  // l'assignatura i els que corresponen a la part de teoria.

  
  vector<Estudiant> vest;
  
  int nest; // nombre d'estudiants del paràmetre implícit
    
  static const int MAX_NEST = 20; // capacitat del paràmetre implícit
  
  int nombre_parcials; // nombre d'exàmens parcials del paràmetre implícit

  int nlab; // nombre de examenes parciales de laboratori
  vector<bool> lab; // subconjunt d'exàmens parcials de laboratori

  int nteo; // nombre d'exàmens parcials de teoria
  vector<bool> teoria; // subconjunt d'exàmens parcials de teoria

  /*
    Invariant de la representació:
    I) vest[0..nest-1] està ordenat creixentment per DNI i no conté dos estudiants amb el mateix DNI 
    II) 0 <= nest <= vest.size() = MAX_NEST
    III) nlab > 0, nteo > 0, nlab + nteo < nombre_parcials, lab.size() == teoria.size() == nombre_parcials 
    IV) Si 0 <= j < nombre_parcials, llavors lab[j] == false or teoria[j] == false.  
        El nombre de posicions j que compleixen lab[j] == true és nlab. 
        El nombre de posicions j tals que teoria[j] == true és nteo. 
    V) Per a tot 0 <= i < nest,
         a) vest[i].consultar_nota_lab() == -1, si vest[i] no s'ha presentat a cap examen parcial de laboratori; o
         b) vest[i].consultar_nota_lab() == n, on 0 <= n <= Estudiant::nota_maxima(), n és igual 
            a la suma de les notes dels exàmens parcials de laboratori als quals vest[i] s'ha presentat dividida 
            per nlab.
    VI) Per a tot 0 <= i < nest,
         a) vest[i].consultar_nota_teoria() == -1, si vest[i] no s'ha presentat a cap examen parcial de teoria; o
         b) vest[i].consultar_nota_teoria() == n, on 0 <= n <= Estudiant::nota_maxima(), n és igual 
            a la suma de les notes dels exàmens parcials de teoria als quals vest[i] s'ha presentat dividida 
            per nteo.
    VII) Per a tot 0 <= i < nest,
         a) vest[i].consultar_nota_global() == -1, si vest[i] no s'ha presentat a cap examen parcial de laboratori o 
            no s'ha presentat a cap examen de teoria; o
         b) vest[i].consultar_nota_global() == n, on 0 <= n <= Estudiant::nota_maxima(), n és igual 
            a la mitjana de la nota de laboratori i de la nota de teoria. 
  */

  
  void ordenar_cjt_estudiants();
  /* Pre: cert */
  /* Post: Els estudiants de vest[0 ... nest - 1] estan ordenats en
     ordre creixent per DNI. */
    
  static int cerca_dicot(const vector<Estudiant>& v, int left, int right, int x);
  /* Pre: v.size() > 0, v[left...right] està ordenat en ordre creixent per DNI, 
     0 <= left <= v.size(), -1 <= right < v.size(), left <= right + 1. */
  /* Post: Si a v[left ... right] hi ha un estudiant amb DNI = x, el
     resultat és la posició que hi ocupa; si no, el resultat és -1. */

  // EXCERCICI
  void avaluacio_global_iesim(int i);
  /* Pre:  1 <= i <= mida()
     L'estudiant i-èsim té notes assignades per a tots els
     exàmens parcials de l'assignatura. La nota de cada examen parcial
     pot ser -1, si l'estudiant no s'ha presentat a aquest examen
     parcial, o una nota vàlida en el rang [0...Estudiant::nota_maxima()]. */
  /* Post: L'estudiant i-èsim passa a tenir assignades les seves notas de 
     laboratori, teoria i global en l'assignatura, que poden ser -1, si es 
     considera que la seva qualificació ha de ser NP (No Presentat), o una 
     nota vàlida en el rang [0...Estudiant::nota_maxima()] que correspon a: 
     1) la suma de les notes de l'estudiant en els exámens parcials de 
     laboratori als quals s'ha presentat dividida per el nombre d'exámens 
     parcials de laboratori de l'assignatura (nlab) en el cas de la nota 
     de laboratori; 2) la suma de les notes de l'estudiant en els exámens 
     parcials de teoria als quals s'ha presentat dividida per el nombre 
     d'exámens parcials de teoria de la assignatura (nteo) en el cas de la 
     nota de teoria; i 3) a la mitjana de la nota de laboratori i de la nota 
     de teoria en el cas de la nota global de l'assignatura. */


  
public:

  //Constructors

  Cjt_estudiants();
  /* Pre: cert */
  /* Post: Crea un conjunt d'estudiants buit. */

  Cjt_estudiants(int n);
  /* Pre: cert */
  /* Post: Crea un conjunt d'estudiants buit amb n exàmens parcials. */

    
  //Consultors
	
  int mida() const;
  /* Pre: cert */
  /* Post: El resultat és el nombre d'estudiants del paràmetre implícit. */

  static int mida_maxima();
  /* Pre: cert */
  /* Post: El resultat és el nombre màxim d'estudiants que pot arribar
     a tenir el paràmetre implícit. */

  int nombre_ex_parcials() const;
  /* Pre: cert */
  /* Post: El resultat és nombre d'exàmens parcials als quals es poden
     presentar els estudiants del paràmetre implícit. */
  
  pair<bool, int> posicio_estudiant(int dni) const;
  /* Pre: cert */
  /* Post: Si existeix un estudiant al paràmetre implícit amb DNI =
     dni, la primera component del resultat és true i la segona és la
     posició que ocupa aquest estudiant en el paràmetre implícit en
     ordre ascendent per DNI; altrament la primera component del
     resultat és false i la segona -1. */
    
  Estudiant consultar_iesim(int i) const;
  /* Pre: 1 <= i <= nombre d'estudiants que conté el paràmetre implícit */
  /* Post: El resultat és l'estudiant i-èsim del paràmetre implícit en ordre 
     ascendent per DNI. */
    
  
  
    //Modificadors
        
  void modificar_iesim(int i, const Estudiant& e);
  /* Pre: 1 <= i <= nombre d'estudiants del paràmetre implícit, l'element 
     i-èsim del conjunt en ordre ascendent per DNI conté un estudiant amb 
     el mateix DNI que e. */
  /* Post: L'estudiant e ha substituït l'estudiant i-èsim del paràmetre 
     implícit. */


  
  // EXCERCICI
  void parcials_no_presentats_aprovats() const;
/* Pre: Tots els estudiants del paràmetre implícit tenen notes
   assignades per a tots els exàmens parcials i també tenen assignada
   la seva nota global en l'assignatura. La nota global i la nota de 
   cada examen parcial poden ser -1, si l'estudiant té la qualificació 
   global No Presentat o si no s'ha presentat a l'examen parcial, o una 
   nota vàlida en el rang [0...Estudiant::nota_maxima()].  */ 
 /* Post: Al canal de sortida estàndard s'han escrit els identificadors 
   dels exàmens parcials als quals no s'ha presentat algun estudiant 
   aprovat en l'assignatura, és a dir, algun estudiant que té una nota 
   global en l'assignatura més gran o igual que 5. Els identificadors 
   d'aquests exàmens parcials estan ordenats en ordre creixent. */
  
  
  // Lectura i escriptura
  
  void llegir();  
  /* Pre: Estan preparats al canal estàndard d'entrada el nombre
     d'exàmens parcials de laboratori, seguit dels seus
     identificadors, el nombre d'exàmens de teoria, seguit dels seus
     identificadors, un enter entre 0 i la mida màxima permesa, que
     representa el nombre d'estudiants que llegirem, i les dades de
     tal nombre d'estudiants. */
  /* Post: El paràmetre implícit conté el conjunt d'estudiants llegits
     del canal estàndard d'entrada i els subconjunts d'exàmens
     parcials de laboratori i de teoria. */
    
  void escriure() const;
  /* Pre: cert */
  /* Post: S'han escrit pel canal estàndard de sortida els estudiants del
     paràmetre implícit en ordre ascendent per DNI. */
};
#endif

