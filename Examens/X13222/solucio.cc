#include "Cjt_estudiants.hh"

void Cjt_estudiants::avaluacio_global_iesim(int i)
{
  i = i - 1;
  double avteo = -1, avlab = -1, avglob = -1;
  for (int j = 0; j < nombre_parcials; ++j) {
    double nota = vest[i].consultar_nota_parcial(j + 1);
    if (teoria[j] and nota != -1) {
      if (avteo == -1) avteo = 0;
      avteo += nota;
    }
    else if (lab[j] and nota != -1) {
      if (avlab == -1) avlab = 0;
      avlab += nota;
    }
  }
  if (avteo != -1) avteo /= nteo;
  if (avlab != -1) avlab /= nlab;
  if (avlab != -1 and avteo != -1) avglob = (avteo + avlab)/2;

  vest[i].assignar_nota_teoria(avteo);
  vest[i].assignar_nota_laboratori(avlab);
  vest[i].assignar_nota_global(avglob);

}


void Cjt_estudiants::parcials_no_presentats_aprovats() const
{
  vector<bool> np(nombre_parcials, false);
  for (int i = 0; i < nest; ++i) {
    if (vest[i].consultar_nota_global() >= 5) {
      for (int j = 0; j < nombre_parcials; ++j) {
        if (vest[i].consultar_nota_parcial(j + 1) == -1) np[j] = true;
      }
    }
  }
  cout << endl << "Examens parcials no presentats per algun estudiant aprovat: ";
  for (int i = 0; i < nombre_parcials; ++i) {
    if (np[i]) cout << ' ' << i + 1;
  }
  cout << endl;
}
