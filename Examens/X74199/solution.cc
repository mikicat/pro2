// Poseu aqui vostre nom, cognoms i grup de laboratori

#include "Cjt_estudiants.hh"

// Traieu els //, /*, etc. adients i completeu les operacions

/* Operaci� afegir_estudiant: el codi que us donem compila sense error i fa
   coses correctes, per� clarament insuficients; analitzeu amb cura quins
   camps de la classe resten per actualitzar
*/

void Cjt_estudiants::afegir_estudiant(const Estudiant &est, bool& b)
  /* Pre: el par�metre impl�cit no est� ple */
  /* Post: b = indica si el p.i. original cont� un estudiant amb el dni d'est;
     si b = fals, s'ha afegit l'estudiant est al par�metre impl�cit */
{
  int x = est.consultar_DNI();
  int i = cerca_dicot(vest,0,nest-1,x);
  b = (i<nest) and (x== vest[i].consultar_DNI());

  // b indica si est hi es a vest; si b es fals, i es la posicio on ha
  // d'anar est per mantenir l'ordre a vest;

  if (not b) {
    for (int j=nest; j>i; --j) vest[j] = vest[j-1];
    vest[i] = est;
    ++nest;
    if (pitjor_becat >= i) ++pitjor_becat;
    if (est.te_nota()) {
      if (pitjor_becat != -1) {
        if (est.consultar_nota() > consultar_iessim(pitjor_becat + 1).consultar_nota()) {
          Estudiant seguent = consultar_iessim(immediatament_millor(pitjor_becat) + 1);
          if (est.consultar_nota() > seguent.consultar_nota()) pitjor_becat = immediatament_millor(pitjor_becat);
          else pitjor_becat = i;
        }
        else if (i < pitjor_becat and est.consultar_nota() == consultar_iessim(pitjor_becat + 1).consultar_nota()) pitjor_becat = i;
        if (num_beques != max_beques) ++num_beques;
      }
      else {
        if (num_beques != max_beques and est.consultar_nota() >= min_nota_beca) ++num_beques, pitjor_becat = i;
      }
    }
  }
}


/* Operaci� immediatament_millor: la linia del cout no pinta res i haurieu de
   treure-la quan comenceu a implementar l'operaci�, per� permet fer
   lliuraments sense error de compilaci�, de forma que l'altra operaci� es
   podria avaluar manualment
*/

int Cjt_estudiants::immediatament_millor(int i) const
/* Pre: 0<=i<nest; vest[i] te nota */
/* Post: el resultat �s la posici� en vest[0..nest-1] de l'estudiant
   immediatament millor que vest[i], si n'hi ha; -1 en cas contrari */
{
  int j = -1;
  double nota = consultar_iessim(i+1).consultar_nota();
  double min_diff = consultar_iessim(i+1).nota_maxima(); // Nota maxima
  for (int k = 1; k <= nest; ++k) {
    if (k != i+1) {
      if (consultar_iessim(k).te_nota()) {
        double current_diff = consultar_iessim(k).consultar_nota() - nota;
        if (current_diff < min_diff) min_diff = current_diff, j = k;
      }
    }
  }

//  cout << vest[i].consultar_nota() << endl;
//  normalment s'hauria de deixar com a comentari

 return j;
}
