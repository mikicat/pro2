#include "Cua_estudiants.hh"

void Cua_estudiants::push(const Estudiant &est)
{
    v[t] = est;
    ++n;
    t = (p + n) % MAX;
    if (est.te_nota() and est.consultar_nota() > notamax) notamax = est.consultar_nota();
    
}

void Cua_estudiants::pop()
{
    if (v[p].te_nota() and v[p].consultar_nota() == notamax) {
        if (p >= t) {
            int i = p + 1;
            int nota = 0;
            while (i < MAX) {
                if (v[i].te_nota() and v[i].consultar_nota() > nota) nota = v[i].consultar_nota();
                ++i;
            }
            i = 0;
            while (i < t) {
                if (v[i].te_nota() and v[i].consultar_nota() > nota) nota = v[i].consultar_nota();
                ++i;
            }
            notamax = nota;
        }
        else {
            int i = p + 1;
            int nota = 0;
            while (i < t) {
                if (v[i].te_nota() and v[i].consultar_nota() > nota) nota = v[i].consultar_nota();
                ++i;
            }
            notamax = nota;
        }
    }
    p = (p + 1) % MAX;
    --n;
}

void Cua_estudiants::escriure() const
{
    if (p >= t) {
        int i = p;
        while (i < MAX) {
            cout << v[i].consultar_DNI() << ' ';
            if (v[i].te_nota()) cout << v[i].consultar_nota() << endl;
            else cout << "NP" << endl;
            ++i;
        }
        i = 0;
        while (i < t) {
            cout << v[i].consultar_DNI() << ' ';
            if (v[i].te_nota()) cout << v[i].consultar_nota() << endl;
            else cout << "NP" << endl;
            ++i;
        }
    }
    else {
        int i = p;
        while (i < t) {
            cout << v[i].consultar_DNI() << ' ';
            if (v[i].te_nota()) cout << v[i].consultar_nota() << endl;
            else cout << "NP" << endl;
            ++i;
        }
    }
}
