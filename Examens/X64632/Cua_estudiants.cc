
#include "Cua_estudiants.hh"

using namespace std;

Cua_estudiants::Cua_estudiants()
{
  n = 0;
  p = 0;
  t = 0;
  notamax = 0;
  v = vector<Estudiant>(MAX);
}

int Cua_estudiants::size() const
{
  return n;
}

int Cua_estudiants::max_size() 
{
  return MAX;
}
 
bool Cua_estudiants::empty() const
{
  return n==0;
}

bool Cua_estudiants::full() const
{
  return n==MAX;
}

Estudiant Cua_estudiants::front() const
{
  return v[p];
}

double Cua_estudiants::nota_maxima() const
{
  return notamax;
}


