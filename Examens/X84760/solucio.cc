#include "Cjt_estudiants.hh"

void Cjt_estudiants::afegir_estudiant(const Estudiant &est, bool& b)
{
  int dni = est.consultar_DNI();
//   int i = cerca_dicot(vest, 0, nest-1, dni);
//   b = (dni == vest[i].consultar_DNI());
  int i = 0;
  b = false;
  while (not b and i < nest) {
      b = (dni == vest[i].consultar_DNI());
      ++i;
  }

  if (not b) {
      int j = nest - 1;
      bool b2 = false;
      while (j >= 0 and not b2) {
          if (dni > vest[j].consultar_DNI()) b2 = true;
          else {
              vest[j + 1] = vest[j];
              --j;
        }
    }
    vest[j + 1] = est;
    i = j + 1;
    ++nest;
    
    if (imax >= i) {
        if (est.te_nota() and est.consultar_nota() >= vest[imax].consultar_nota()) imax = i;
        else ++imax;
    }
    else if (est.te_nota() and imax == -1) imax = i;
    else if (est.te_nota() and est.consultar_nota() > vest[imax].consultar_nota()) imax = i;
    
    
  }
}

void Cjt_estudiants::eliminar_estudiants_sense_nota()
{
    if (imax == -1) nest = 0;
    else {
        int i = 0;
        while (i < nest) {
            if (not vest[i].te_nota()) {
                for (int j = i; j < nest -1; ++j) {
                    vest[j] = vest[j+1];
                }
                if (imax >= i) --imax;
                --nest;
            }
            else ++i;
        }
    }
    
}
