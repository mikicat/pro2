#include "Cjt_estudiants.hh"

int Cjt_estudiants::posicio_nota(double nota_b) const
{
  for (int i = primer_est_amb_nota; i < nest; ++i) {
    if (vest[i].consultar_nota() >= nota_b) return i;
  }
  return nest;
}

void Cjt_estudiants::afegir_estudiant(const Estudiant &est, bool &trobat)
{
  int dni = est.consultar_DNI();
  int i = cerca_dicot_per_dni(vest, 0, nest-1, dni);
  trobat = (i < nest) and (dni == vest[i].consultar_DNI());

  if (not trobat) {
    if (est.te_nota()) {
      bool b2 = false;
      int j = nest - 1;
      while (j >= primer_est_amb_nota and not b2) {
        if (est.consultar_nota() > vest[j].consultar_nota()) b2 = true;
        else if (est.consultar_nota() == vest[j].consultar_nota() and dni > vest[i].consultar_DNI()) b2 = true;
        else {
          vest[j + 1] = vest[j];
          --j;
        }
      }
      vest[j + 1] = est;
    }
    else {
      int j = nest - 1;
      bool b2 = false;
      while (j >= 0 and not b2) {
        if (not vest[j].te_nota() and dni > vest[j].consultar_DNI()) b2 = true;
        else {
          vest[j + 1] = vest[j];
          --j;
        }
      }
      vest [j + 1] = est;

      ++primer_est_amb_nota;
    }
    ++nest;
  }
}

void Cjt_estudiants::escriure_i(double nota_i, double nota_s) const
{
  int start_pos = posicio_nota(nota_i);
  if (start_pos < nest) {
    bool stop = false;
    int i = start_pos;
    while (not stop) {
      cout << vest[i].consultar_DNI() << ' ' << vest[i].consultar_nota() << endl;
      ++i;
      if (i == nest or vest[i].consultar_nota() > nota_s) stop = true;
    }
  }
}
