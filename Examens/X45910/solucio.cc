// Poseu aqui vostre nom, cognoms i grup de laboratori



#include "Cjt_estudiants.hh"

// Traieu els comentaris // abans de posar codi a les funcions

void Cjt_estudiants::unir_conjunts(const Cjt_estudiants & c){
/* Pre: tots els dni de c hi són al p.i. */
/* Post: cada Estudiant del paràmetre implicit conté la millor nota entre
   l'original i la que pugui tenir a c */
   int i = 0;
   int j = 0;
   while (j < c.mida()) {
     if (vest[i].consultar_DNI() == c.vest[j].consultar_DNI()) {
       Estudiant actual = c.vest[j];
       if (actual.te_nota()) {
         if (vest[i].te_nota()) {
           if (actual.consultar_nota() > vest[i].consultar_nota()) {
             vest[i] = actual;
           }
         }
         else {
           vest[i] = actual;
         }
         if (imax == -1) imax = i;
         else if (vest[imax].consultar_nota() < actual.consultar_nota()) imax = i;
         else if (vest[imax].consultar_nota() == actual.consultar_nota() and i < imax) imax = i;
       }
       ++i, ++j;
     }
     else ++i;
   }
}


void Cjt_estudiants::actualitzar_conjunt() {
/* Pre: a l'entrada estàndar hi ha una seqüència d'Estudiant (seguida d'un
   Estudiant amb dni 0 que no forma part de la seqüència), amb els dni
   ordenats creixentment i tots hi són al p.i. */
/* Post: cada Estudiant del paràmetre implicit conté la millor nota entre l'original
   i la que pugui tenir a la seqüència */
   bool stop = false;
   while (not stop) {
     Estudiant est;
     est.llegir();
     if (est.consultar_DNI() == 0) stop = true;
     else {
       int p = 0;
       bool b = false;
       while (p < nest and not b) {
         if (vest[p].consultar_DNI() == est.consultar_DNI()) b = true;
         else ++p;
       }
       if (b) {
         if (est.te_nota()) {
           if (vest[p].te_nota())
           {
             if (est.consultar_nota() > vest[p].consultar_nota()) {
               vest[p] = est;
             }
           }
          else {
            vest[p] = est;
          }
          if (imax == -1) imax = p;
          else if (vest[imax].consultar_nota() < est.consultar_nota()) imax = p;
          else if (vest[imax].consultar_nota() == est.consultar_nota() and p < imax) imax = p;
         }
       }
     }
   }
}
