#include "ListaPalabras.hh"

void ListaPalabras::anadir_palabra(const Palabra &p)
{
  bool b;
  int i;
  buscar_palabra_posicion(p, b, i);

  int longi = p.long_pal();
  if (b) {
    ++paraules[i].freq;
    ++suma_frec_long[longi - 1];
  }
  else {
    paraules[i].par = p;
    paraules[i].freq = 1;
    ++suma_frec_long[longi - 1];
    ++npar_long[longi - 1];
    ++nparaules;
  }
}

void ListaPalabras::borrar_palabra(const Palabra &p)
{
  bool b;
  int i;
  buscar_palabra_posicion(p, b, i);

  if (b) {
    int longi = p.long_pal();
    --suma_frec_long[longi - 1];
    --paraules[i].freq;

    if (paraules[i].freq == 0) {
      --npar_long[longi - 1];
      for (int j = i; j < nparaules-1; ++j) {
        paraules[j] = paraules[j + 1];
      }
      --nparaules;
    }

  }

}

void ListaPalabras::buscar_palabra_posicion(const Palabra &p, bool &b, int &i) const
{
  b = false;
  i = 0;
  while (not b and i < nparaules) {
    if (p.long_pal() == paraules[i].par.long_pal()) {
      int j = 1;
      b = true;
      while (j <= p.long_pal() and b) {
        if (p.consultar_letra(j) != paraules[i].par.consultar_letra(j)) b = false;
        else ++j;
      }
      if (not b) ++i;
    }
    else ++i;
  }
}
