
#include "Cua_estudiants.hh"

using namespace std;

Cua_estudiants::Cua_estudiants()
{
  n = 0;
  p = 0;
  t = 0;
  nest_amb_nota = 0;
  suma = 0;
  v = vector<Estudiant>(MAX);
}

int Cua_estudiants::size() const
{
  return n;
}

int Cua_estudiants::max_size() 
{
  return MAX;
}
 
bool Cua_estudiants::empty() const
{
  return n==0;
}

bool Cua_estudiants::full() const
{
  return n==MAX;
}

Estudiant Cua_estudiants::front() const
{
  return v[p];
}

int Cua_estudiants::presentats() const
{
  return nest_amb_nota;
}

double Cua_estudiants::nota_mitjana() const
{
  return suma/nest_amb_nota;
}


