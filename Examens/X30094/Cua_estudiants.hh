#ifndef CUA_EST_HH
#define CUA_EST_HH

#include <vector>
#include <iostream>
#include "Estudiant.hh"

using namespace std;

class Cua_estudiants {

private:

  // Representa una cua d'estudiants (amb mida m�xima MAX)  
  
  vector<Estudiant> v; // contenidor pels estudiants de la cua

  int n; // nombre d'estudiants de la cua

  static const int MAX = 10; // nombre m�xim d'estudiants de la cua

  int p; // serveix per a poder accedir al primer de la cua
  int t; // serveix per a poder accedir al final de la cua

  int nest_amb_nota; // nombre d'estudiants amb nota de la cua
  double suma; // suma de les notes dels estudiants amb nota de la cua

  /* 
     Invariant de la representaci�: 
     - 0 <= n <= MAX = v.size()
     - 0 <= p <= MAX-1 
     - t = (p + n) % MAX 
     - n > 0  => 
            si p < t llavors el subvector v[p..t-1] cont� els estudiants 
               de la cua ordenats per ordre d'arribada a la cua, on
               v[p] �s el primer (el m�s antic) i v[t-1] �s el darrer (el m�s recent),
          i si p >= t llavors la concatenaci� dels subvectors v[p..MAX-1]v[0..t-1] 
               cont� els estudiants de la cua ordenats per ordre d'arribada a la cua, on
               v[p] �s el primer, v[t-1] �s el darrer si t!=0 i v[MAX-1] �s el darrer si t=0
     - 0 <= nest_amb_nota <= n, nest_amb_nota indica quants estudiants de la cua tenen nota
     - suma �s la suma de les notes dels estudiants amb nota de la cua 
         i ser� zero si cap d'ells en t� o si la cua �s buida
  */
 
public:

  //Constructores

  Cua_estudiants();
  /* Pre: cert */
  /* Post: crea una cua d'estudiants buida */

  //Modificadores

  void push(const Estudiant &est);
  /* Pre: el par�metre impl�cit no est� ple */
  /* Post: el par�metre impl�cit �s com el par�metre impl�cit original amb 
           est afegit com a darrer element */

  void pop();
  /* Pre: el par�metre impl�cit no est� buit */
  /* Post: el par�metre impl�cit �s com el par�metre impl�cit original per� 
         sense el primer element afegit al par�metre impl�cit original */

  //Consultores
	
  int size() const;
  /* Pre: cert */
  /* Post: El resultat �s el nombre d'estudiants del par�metre impl�cit */

  static int max_size();
  /* Pre: cert */
  /* Post: el resultat �s el nombre m�xim d'estudiants que pot arribar
     a tenir el par�metre impl�cit */

  Estudiant front() const;
  /* Pre: el par�metre impl�cit no est� buit */
  /* Post: el resultat �s l'estudiant m�s antic afegit al par�metre impl�cit */

  bool empty() const;
  /* Pre: cert */
  /* Post: el resultat indica si el par�metre impl�cit est� buit */

  bool full() const;
  /* Pre: cert */
  /* Post: el resultat indica si el par�metre impl�cit est� ple */

  int presentats() const;
  /* Pre: cert */
  /* Post: El resultat �s el nombre d'estudiants amb nota del par�metre impl�cit */

  double nota_mitjana() const;
  /* Pre: el par�metre impl�cit t� algun estudiant amb nota */
  /* Post: el resultat �s la nota mitjana dels estudiants amb nota del par�metre impl�cit */  

  // Escriptura
	
  void escriure() const;
  /* Pre: cert */
  /* Post: s'han escrit pel canal est�ndar de sortida els estudiants del
     par�metre impl�cit per ordre d'arribada a la cua (del primer al darrer) */
};

#endif

