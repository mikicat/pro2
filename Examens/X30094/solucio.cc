#include "Cua_estudiants.hh"

void Cua_estudiants::push(const Estudiant &est)
{
  v[t] = est;
  ++n;
  t = (p + n) % MAX;
  if (est.te_nota()) {
    ++nest_amb_nota;
    suma += est.consultar_nota();
  }
}

void Cua_estudiants::pop()
{
  if (v[p].te_nota()) {
    --nest_amb_nota;
    suma -= v[p].consultar_nota();
  }
  p = (p + 1) % MAX;
  --n;
}

void Cua_estudiants::escriure() const
{
  for (int i = 0; i < n; ++i) {
    int id = (p + i) % MAX;
    cout << v[id].consultar_DNI() << ' ';
    if (v[id].te_nota()) cout << v[id].consultar_nota();
    else cout << "NP";
    cout << endl;
  }
}
