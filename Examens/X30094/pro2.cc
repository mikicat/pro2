#include <iostream>
#include "Cua_estudiants.hh"

using namespace std;

int main() {
  Cua_estudiants c;
  int op; 
  cin >> op;
  while (op != -6) {
    Estudiant est;  
    switch (op) { 
    case -1:   // afegir estudiant a la cua
      est.llegir();
      if (c.full())  cout << "La cua d'estudiants es plena" << endl << endl;
      else c.push(est);
      break;
    case -2:   // esborrar primer estudiant de la cua
      if (c.empty())  cout << "La cua d'estudiants es buida" << endl << endl;
      else c.pop();   
      break; 
    case -3:   // consultar primer estudiant de la cua
      if (c.empty())  cout << "La cua d'estudiants es buida" << endl << endl;
      else {
        est = c.front();   
        cout << "Primer estudiant:" << endl;
        est.escriure();
      }
      cout << endl;
      break;
    case -4:   // escriure cua
      cout << "Cua:" << endl;
      c.escriure();
      cout << endl;
      break;
    case -5:   // consultar nota mitjana dels estudiants amb nota de la cua
      if (c.presentats()==0)  cout << "La cua no te cap estudiant amb nota" << endl << endl;
      else  cout << "La nota mitjana dels estudiants amb nota es: " << c.nota_mitjana() << endl << endl;
    }
    cin >> op;
  }
}

