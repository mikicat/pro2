#include "Cjt_estudiants.hh"

void Cjt_estudiants::avaluacio_global_iesim(int i)
{
  double np = 0;
  double av = 0;
  int j = 0;
  while (j < nombre_parcials and np <= 0.5) {
    double nota = vest[i - 1].consultar_nota_parcial(j + 1);
    if (nota == -1) np += pesos_parcials[j];
    else av += pesos_parcials[j] * nota;
    ++j;
  }
  if (np > 0.5) vest[i - 1].assignar_nota_global(-1);
  else vest[i - 1].assignar_nota_global(av);
}
/* Pre:  1 <= i <= mida()
   L'estudiant i-èsim té notes assignades per a tots els exàmens parcials de
   l'assignatura. La nota de cada examen parcial pot ser -1, si l'estudiant
   no s'ha presentat a aquest examen parcial, o una nota vàlida en el rang
   [0...Estudiant::nota_maxima()]. */
/* Post: L'estudiant i-èsim passa a tenir assignada la seva nota global en
   l'assignatura, que pot ser -1, si es considera que la seva qualificació
   global ha de ser NP (No Presentat), o una nota vàlida en el rang
   [0...Estudiant::nota_maxima()], que correspon a la suma ponderada de les
   seves notes en els exàmens parcials de l'assignatura als quals s'ha
   presentat. */

void Cjt_estudiants::parcials_presentats_aprovats() const
{
  vector<bool> examens(nombre_parcials, true);
  for (int i = 0; i < nest; ++i) {
    if (vest[i].consultar_nota_global() >= 5) {
      for (int j = 1; j <= nombre_parcials; ++j) {
        if (vest[i].consultar_nota_parcial(j) == -1) examens[j - 1] = false;
      }
    }
  }

  cout << endl << "Examens parcials presentats per tots els estudiants aprovats: ";
  for (int i = 0; i < nombre_parcials; ++i) {
    if (examens[i]) cout << ' ' << i + 1;
  }
  cout << endl;

}
   /* Pre: Tots els estudiants del paràmetre implícit tenen notes assignades per
      a tots els exàmens parcials i també tenen assignada la seva nota global en
      l'assignatura. La nota de cada examen parcial pot ser -1, si l'estudiant no
      s'ha presentat a aquest examen parcial, o una nota vàlida en el rang
      [0...Estudiant::nota_maxima()]. */
   /* Post: Al canal de sortida estàndard s'han escrit els identificadors dels
      exàmens parcials als quals s'han presentat tots els estudiants que tenen
      una nota global en l'assignatura més gran o igual a 5. Els identificadors
      d'aquests exàmens parcials estan ordenats en ordre creixent. */
