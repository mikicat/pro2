/* Poseu aqui vostre nom, cognoms i grup de laboratori:


*/

#include "Cjt_estudiants.hh"
#include "solution.hh"

/* Traieu els comentaris "//" i completeu les operacions

   Recordeu que no podeu fer servir elements privats de les classes
   {\tt Cjt\_estudiants} i {\tt Es\-tu\-diant}
*/

void extreure_reavaluables(const Cjt_estudiants& c1, const Cjt_estudiants& c2,
                          double x, double y, double z, Cjt_estudiants& reava)
  /* Pre: c1 i c2 contenen els mateixos estudiants, reava es buit,
          0<=x,y,z<=Estudiant::nota_maxima(), x<=y */
  /* Post: el conjunt reava est� format pels estudiants reavaluables de c1 i c2
           respecte a x,y,z, tots sense nota */
{
  for (int i = 0; i < c1.mida(); ++i) {
    if (c1.consultar_iessim(i + 1).te_nota() and c2.consultar_iessim(i + 1).te_nota()) {
      double nota1 = c1.consultar_iessim(i + 1).consultar_nota();
      double nota2 = c2.consultar_iessim(i + 1).consultar_nota();
      if (nota1 >= x and nota1 <= y and nota2 >= z) {
        Estudiant est(c1.consultar_iessim(i + 1).consultar_DNI());
        bool b;
        reava.afegir_estudiant(est, b);
      }
    }
  }
}
