#include "Cjt_estudiants.hh"
#include "solution.hh"
#include <algorithm>

/* Traieu els comentaris "//" i completeu les operacions

   Recordeu que no podeu fer servir elements privats de les classes
   {\tt Cjt\_estudiants} i {\tt Es\-tu\-diant}
*/

void escriure_becats(const Cjt_estudiants& c) {
  /* Pre: cert */
  /* Post: s'han escrit pel canal est�ndar de sortida els estudiants
     de c amb beca en ordre ascendent per DNI */
  Estudiant min = c.becat_menys_prioritari();
  for (int i = 0; i < c.mida(); ++i) {
    bool better = false;
    Estudiant best = c.consultar_iessim(i + 1);
    if (best.consultar_nota() > min.consultar_nota()) better = true;
    else if (best.consultar_nota() == min.consultar_nota() and best.consultar_DNI() >= min.consultar_DNI()) better = true;
    if (better) cout << best.consultar_DNI() << ' ' << best.consultar_nota() << endl;
  }
}
