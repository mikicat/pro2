#include "Cjt_estudiants.hh"

void Cjt_estudiants::afegir_estudiant(const Estudiant &est, bool& b)
{
  b = false;
  int dni = est.consultar_DNI();
  int i = 0;
  while (i < nest and not b) {
    if (dni == vest[i].consultar_DNI()) b = true;
    else ++i;
  }

  if (not b) {
    i = nest - 1;
    bool b2 = false;
    while (i >= 0 and not b2) {
      if (dni > vest[i].consultar_DNI()) b2 = true;
      else {
        vest[i+1] = vest[i];
        --i;
      }
    }
    vest[i + 1] = est;
    if (est.te_nota()) {
      int nota = int(est.consultar_nota());
      if (nota == 10) ++intervals[nota - 1];
      else ++intervals[nota];
    }
    ++nest;
  }
}

void Cjt_estudiants::esborrar_estudiant(int dni, bool& b)
{
  b = false;
  int i = 0;
  while (i < nest and not b) {
    if (dni == vest[i].consultar_DNI()) b = true;
    else ++i;
  }

  if (b) {
    if (vest[i].te_nota()) {
      int nota = int(vest[i].consultar_nota());
      if (nota == 10) --intervals[nota - 1];
      else --intervals[nota];
    }

    for (int j = i; j < nest - 1; ++j) {
      vest[j] = vest[j + 1];
    }
    --nest;
  }
}
